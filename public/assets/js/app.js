$(document).ready(function($){
    //loader
    var loader = $('.loader');
    loader.addClass('fade-out');
    // datepicker
    $(document).ready(function(){
        $('.secteur').select2();
        $('.contrat').select2();
        $('.pays').select2();
    })

    //counteur up
    $(".counter").counterUp({
        time: 1200,
        delay: 10 ,
    })
    //banner ladatema
    var navbar= $('.header-inner')
    $(window).scroll(function ()
    {
        if($(window).scrollTop() <= 10)
        {
            navbar.removeClass('navbar-scroll')
        }else
        {
            navbar.addClass('navbar-scroll')
        }
    })



    //carousel

    $('.owl-5').owlCarousel({
        loop:true,
        center:true,
        items: 3 ,
        dots: false,
        autoplay: true,
        autoplayTimeout: 8500,
        smartSpeed: 450,
        margin:40,
        responsive:{
            0:{
                items:1
            },
            760:{
                items:2
            },
            1000:{
                items:3
            }
        }
    })

        $('.owl-6').owlCarousel({
            loop:true,
            center:false,
            items: 1 ,
            dots: false,
            autoplay: true,
            autoplayTimeout: 8500,
            smartSpeed: 450,
            margin:80,
            responsive:{
                0:{
                    items:1
                },
                760:{
                    items:1
                },
                1000:{
                    items:1
                }
            }
        })
        $('.owl-7').owlCarousel({
            loop:true,
            center:false,
            dots: false,
            items: 2,
            autoplay: true,
            autoplayTimeout: 8500,
            smartSpeed: 450,
            margin:80,
            responsive:{
                0:{
                    items:1
                },
                760:{
                    items:2
                },
                1000:{
                    items:2
                }
            }
        })
    //gallery

    $(".gallery a").click(function (event) {
        var pop_img = $(this).attr('href');
        $("body").append('<div id="close"class="pop_img_bg "> <div class="pop_img"> <img src="' + pop_img + '" /></div> </div>');

        $('.pop_img_bg').click(function(event){
            setTimeout(function(){
                $('.pop_img_bg').remove()
            })
        });
        $(".pop_img").Click(function (event) {
            return false;
        });
        return false
    });

});

ScrollReveal().reveal('._carousel',{
    delay: 500
});
ScrollReveal().reveal('#navbar',{delay: 400});
ScrollReveal().reveal('#section1',{delay: 500});
ScrollReveal().reveal('#section2',{delay: 500});
ScrollReveal().reveal('#title1',{delay: 600});
ScrollReveal().reveal('.title',{delay: 600});
ScrollReveal().reveal('#title2',{delay: 700});
ScrollReveal().reveal('#btn',{delay: 600});
ScrollReveal().reveal('#btn2',{delay: 700});
ScrollReveal().reveal('#banner-info',{delay: 600})
ScrollReveal().reveal('.info1',{delay: 600})
ScrollReveal().reveal('.info2',{delay: 700});
ScrollReveal().reveal('.banner-img',{delay: 600});
ScrollReveal().reveal('.partenaire',{delay: 600});
ScrollReveal().reveal('.banner-logo',{delay: 700});
ScrollReveal().reveal('.offre',{delay: 600});
ScrollReveal().reveal('.banner-info',{delay: 700});
ScrollReveal().reveal('.banner-counter',{delay: 600});
ScrollReveal().reveal('.banner-counter .col-md',{delay: 700});
ScrollReveal().reveal('.footer,.avis',{delay: 500});
ScrollReveal().reveal('#all',{delay: 500 });
ScrollReveal().reveal('.offre',{delay: 500 })
