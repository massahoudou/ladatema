<?php

namespace App\Form;

use App\Entity\Formation;
use FOS\CKEditorBundle\Form\Type\CKEditorType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class FormationType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('titre')
            ->add('contenu',TextareaType::class)
            ->add('date_debut',DateType::class,[

                'widget' => 'single_text',
                'attr' => [
                    'class' => 'form-control js-datepicker1',
                ],
            ])
            ->add('date_fin',DateType::class,[
                'required' => false,
                'widget' => 'single_text',
                'attr' => [
                    'class' => 'form-control js-datepicker2',
                ],
            ])
            ->add('etablissement')
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Formation::class,
        ]);
    }
}
