<?php


namespace App\Form;


use App\Data\SearchData;
use App\Entity\Catcontrat;
use App\Entity\Secteur;
use Symfony\Component\Form\Extension\Core\Type\CountryType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\Regex;

class SearchType extends  AbstractType
{

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('q', \Symfony\Component\Form\Extension\Core\Type\TextType::class,[
                'label'=> false,
                'required' => false,
                'constraints'=> [
                   new Regex('/^\w+/',"vontre entré ne correspond pas !")
                ],
                'attr' => [
                    'class' => 'search2',
                    'placeholder' => 'Recherche',
                    ]
            ])
            ->add('categorie',EntityType::class,[
                'label' => false,
                'required' => false,
                'class' => Catcontrat::class,
                'expanded' => true,
                'multiple'=> true,
            ])
            ->add('secteur',EntityType::class,[
                'label' => false,
                'required' => false,
                'class' => Secteur::class,
                'expanded' => true,
                'multiple'=> true,
            ])
            ->add('pays',CountryType::class,[
                'label' => 'Pays ',
                'required' => false,
                'preferred_choices' => ['TG', 'Togo'],
                'attr' => [
                    'placeholder' => 'Pays',
                    'class' => ' contrat',
                    ]
            ])
            // ->add('min',IntegerType::class,[
            //     'label' => 'salaire min',
            //     'required'=> False,
            //     'attr' => [
            //         'placeholder' => 'Min',
            //         'class' => '',
            //         ]
            // ])
            // ->add('max',IntegerType::class,[
            //     'label' => 'salaire max',
            //     'required' => false,
            //     'attr' => [
            //         'placeholder' => 'Max',
            //         'class' => '',
            //     ]
            // ])

            ->add('etud',ChoiceType::class,[
                'multiple' => false,
                'required' => false,
                'label' => false,
                'expanded' => true,
                'choices' => [
                    'CEPD' =>  'cepd',
                    'BEPC' =>  'bepc',
                    'Bac' =>  'bac',
                    'Bac + 2' =>  'bac + 2',
                    'Bac + 3' => 'bac + 3',
                    'Bac + 4' => 'bac + 4',
                    'Bac + 5 ou plus ' =>'bac + 5',
                    'Non precisé' => 'non',
                ],
            ])
            ->add('exp',ChoiceType::class,[
                'multiple' => false,
                'required' => false,
                'label' => false,
                'expanded' => true,
                'choices' => [
                    'Débutant' => 'debutant',
                    '3 mois ' => '3 mois',
                    '6 mois ' => '6 mois',
                    '1 an ' => '1 an',
                    '2 ans ' => '2 ans',
                    '3 ans ' => '3 ans',
                    '4 ans ' => '4 ans',
                    '5 ans ' =>'5 ans',
                    '6 ans ' => '6 ans',
                    '7 ans ' => '7 ans',
                    '8 ans ' => '8 ans',
                    '9 ans ' => '9 ans',
                    '10 ans ou plus  ' => '10 ans',
                    'Nom précisé' =>  'non',
                ],
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => SearchData::class,
            'method' => 'GET',
            'csrf_protection' => false,
        ]);
    }

    public function getBlockPrefix()
    {
      return '';
    }
}