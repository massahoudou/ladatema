<?php

namespace App\Form;

use App\Entity\Motdirecteur;
use FOS\CKEditorBundle\Form\Type\CKEditorType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Vich\UploaderBundle\Form\Type\VichFileType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;

class MotdirecteurType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('titre')
            ->add('message',CKEditorType::class,[
              'required' => true,
              'attr' => [
                'class' => 'editor',
              ]
            ])
            ->add('directeur')
            ->add('imageFile',VichFileType::class,[
                'label' => 'Votre image',
                'required' => false,
                'asset_helper' =>  true,
                'allow_delete' => true ,

            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Motdirecteur::class,
        ]);
    }
}
