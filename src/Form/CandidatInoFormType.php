<?php

namespace App\Form;

use App\Entity\Candidat;
use App\Entity\Secteur;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\CountryType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\Length;

class CandidatInoFormType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('nom',TextType::class,[
                'label' => 'Nom',
                'attr'=> [
                    'placeholder' => 'Votre Nom'
                ]
            ])
            ->add('prenom',TextType::class,[
                'attr'=> [
                    'placeholder' => 'Votre prenom'
                ]
            ])
            ->add('telephone',IntegerType::class,[
                'required'=> false,
                 'label' => 'Téléphone',
                 'attr'=> [
                     'placeholder' => 'Votre téléphone'
                 ]
            ])
            ->add('niveau',ChoiceType::class,[
                'required'=> false,
                'multiple' => false,
                'choices' => [
                    'CPD' =>  'cpd',
                    'BEPC' =>  'bepc',
                    'Bac' =>  'bac',
                    'Bac + 2' =>  'bac + 2',
                    'Bac + 3' => 'bac + 3',
                    'Bac + 4' => 'bac + 4',
                    'Bac + 5 ou plus ' =>'bac + 5',
                    'Non precisé' => 'non',
                ],
                'label' => 'Niveau d\'etude',
            ])
            ->add('metier',TextType::class,[
                'required'=> false,
                'label' => 'Metier',
                'attr'=> [
                    'placeholder' => 'Votre Metier rechercher'
                ]
            ])
            ->add('pays', CountryType::class,[
                'label' => 'Votre pays',
                'preferred_choices' => ['TG', 'Togo'],
            ])
            ->add('secteur',EntityType::class,[
                'label' => "Secteur d'activité",
                'required' => false,
                'class' => Secteur::class,
                'expanded' => false,
                'multiple'=> true,
                'choice_label' => 'nom',
                'attr' => ['class' => 'secteur']
            ])
            ->add('anexp',ChoiceType::class,[
                'required'=> false,
                'multiple' => false,
                'label' => 'Année d\'expèrience',
                'choices' => [
                    'Débutant' => 'debutant',
                    '3 mois ' => '3 mois',
                    '6 mois ' => '6mois',
                    '1 an ' => '1 an',
                    '3 ans ' => '3 ans',
                    '4 ans ' => '4 ans',
                    '5 ans ' =>'5 ans',
                    '6 ans ' => '6 ans',
                    '7 ans ' => '7 ans',
                    '8 ans ' => '8 ans',
                    '9 ans ' => '9 ans',
                    '10 ans ou plus  ' => '10 ans',
                    'Nom précisé' =>  'non',
                ],
            ])
            ->add('description' ,TextareaType::class,[
                'label' => 'Decrivez-vous',
            ])
            ->add('isavailable',null,[
                'label' => 'Etes vous disponible ?',
            ])
            ->add('ville',null,[
                'label' => 'Ville',
            ])
            ->add('sexe',ChoiceType::class,[
                'label' => 'Sexe',
                'required'=> false,
                'choices' => [
                    'Masculin'=> 'masculin',
                    'Feminin'=> 'féminin'
                ],
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Candidat::class,
        ]);
    }
}
