<?php

namespace  App\Form;


use App\Data\SearchData;
use App\Entity\Catcontrat;
use App\Entity\Secteur;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\Extension\Core\Type\CountryType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\Regex;

class SearchType2 extends AbstractType
{

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder /*->add('secteur',EntityType::class,[
           'label' => false,
           'attr' => [
               'placeholder'=>'Secteur d\' activité',
               'class' => 'rounded-0 secteur'
           ],
           'required' => false,
           'class' => Secteur::class,
           'multiple' => true,
       ])*/
            ->add('q', \Symfony\Component\Form\Extension\Core\Type\TextType::class, [
                'label' => false,
                'required' => false,
                'constraints' => [
                  new  Regex('/^[A-Z]+$/i',"vontre entré ne correspond pas !")
                ],
                'attr' => [
                    'class' => 'rounded-0 search shadow ',
                    'placeholder' => 'Mot clef..'
                ]
            ])
            ->add('pays', CountryType::class, [
                'label' => false,
                'preferred_choices' => ['TG', 'Togo'],
                'attr' => [
                    'placeholder' => 'Pays',
                    'class' => 'contrat',
                ]
            ])
            
            /*  ->add('exp',ChoiceType::class,[
               'multiple' => false,
               'required' => false,
               'label' => 'Expérience',
               'choices' => [
                   'Débutant' => 0,
                   '3 mois ' => 1,
                   '6 mois ' => 2,
                   '1 an ' => 3,
                   '3 ans ' => 4,
                   '4 ans ' => 5,
                   '5 ans ' => 6,
                   '6 ans ' => 7,
                   '7 ans ' => 8,
                   '8 ans ' => 9,
                   '9 ans ' => 10,
                   '10 ans ou plus  ' => 11,
                   'Nom précisé' =>  12,
               ],
           ])
           ->add('etud',ChoiceType::class,[
               'multiple' => false,
               'required' => false,
               'label' => 'Niveau d\'étude',
               'choices' => [
                   'CPD' =>  1,
                   'BEPC' =>  2,
                   'Bac' =>  3,
                   'Bac + 2' =>  4,
                   'Bac + 3' => 5,
                   'Bac + 4' => 6,
                   'Bac + 5 ou plus ' => 7,
                   'Non precisé' => 0,
               ]
           ])*/;
    }
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => SearchData::class,
            'method' => 'GET',
            'csrf_protection' => false,
        ]);
    }

    public function getBlockPrefix()
    {
        return '';
    }
}
