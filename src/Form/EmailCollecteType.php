<?php

namespace App\Form;

use App\Entity\Email;
use App\Entity\Newsletter\Categories;

use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class EmailCollecteType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('email',EmailType::class,[
                'attr'=> [
                    'placeholder'=> 'Votre adresse email '
                ]
            ])
            ->add('categories',EntityType::class,[
                'class' => Categories::class,
                'choice_label' => 'nom',
                'multiple' => true ,
                'expanded' => true,
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => Email::class,
        ]);
    }
}
