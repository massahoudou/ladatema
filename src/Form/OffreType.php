<?php

namespace App\Form;

use App\Entity\Offre;
use App\Entity\Recruteur;
use App\Entity\Secteur;
use FOS\CKEditorBundle\Form\Type\CKEditorType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\CountryType;
use Symfony\Component\Form\Extension\Core\Type\DateTimeType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Vich\UploaderBundle\Form\Type\VichImageType;
use \Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Validator\Constraints\Country;
use Vich\UploaderBundle\Form\Type\VichFileType;

class OffreType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('titre',TextType::class,[
                'label'=> 'Titre de l\'offre',
                'required'=> true,
                'attr'=> [
                    'minMessage'=>10
                ]
            ])
            ->add('description',CKEditorType::class,[
                'label'=> 'Détailler plus l\'offre',
                'required' => true,
                'attr'=> [
                  'class' => 'editor',
                ]
            ])
            ->add('recruteur',EntityType::class,[
                'class' => Recruteur::class,
                'choice_label' => 'nomemtreprise',
                'placeholder' => 'Selectionnez un recruteur',
                'empty_data'  => null,
                'required' => false ,
                'multiple' => false,
                'attr' => ['class' => 'secteur']

            ])
            ->add('nombreposte',IntegerType::class,[
                'label'=> 'Nombre de poste',
            ])
            ->add('experience',ChoiceType::class,[
                'multiple' => false,
                'required' => true,
                'label' => 'Expérience',
                'choices' => [
                    'Débutant' => 'debutant',
                    '3 mois ' => '3 mois',
                    '6 mois ' => '6mois',
                    '1 an ' => '1 an',
                    '2 ans ' => '2 ans',
                    '3 ans ' => '3 ans',
                    '4 ans ' => '4 ans',
                    '5 ans ' =>'5 ans',
                    '6 ans ' => '6 ans',
                    '7 ans ' => '7 ans',
                    '8 ans ' => '8 ans',
                    '9 ans ' => '9 ans',
                    '10 ans ou plus  ' => '10 ans',
                    'Nom précisé' =>  'non',
                ],
            ])
            ->add('ville',TextType::class,[
              'required' => true,
            ])

            ->add('etude',ChoiceType::class,[
                'multiple' => false,
                'required' => true,
                'label' => 'Niveau d\'étude',
                'choices' => [
                    'CEPD' =>  'cepd',
                    'BEPC' =>  'bepc',
                    'Bac' =>  'bac',
                    'Bac + 2' =>  'bac + 2',
                    'Bac + 3' => 'bac + 3',
                    'Bac + 4' => 'bac + 4',
                    'Bac + 5 ou plus ' =>'bac + 5',
                    'Non precisé' => 'non',
                ],
            ])

            ->add('delai',DateType::class,[
                'required' => true,
                'widget' => 'single_text',
                'attr' => [
                    'class' => 'form-control',
                ],
            ])
            ->add('catcontrat',null, [
                'required' => true,
                'attr' => [ 'class' => 'contrat form-control']
            ])
            ->add('secteur', EntityType::class,[
                'class' => Secteur::class,
                'required' => true ,
                'choice_label' => 'nom',
                'multiple' => true,
                'attr' => ['class' => 'secteur']
                ])
            //->add('salaire')
            ->add('pays',CountryType::class,[
                'label' => 'Pays concerner',
                'preferred_choices' => ['TG', 'Togo'],
            ])
            ->add('imageFile',VichFileType::class,[
                'label' => 'Votre image',
                'required' => false,
                'asset_helper' =>  true,
                'allow_delete' => true ,
            ])
            ->add('archive',null,[
                'label' => 'Archiver l\'offre d\'emploi',
            ])

        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Offre::class,
        ]);
    }
}
