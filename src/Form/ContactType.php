<?php

namespace App\Form;

use App\Entity\Contact;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class ContactType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('nom',\Symfony\Component\Form\Extension\Core\Type\TextType::class,[
                'label'=>'Nom',
                'attr'=> [

                ]
            ])
            ->add('telephone',IntegerType::class,[
                'required'=> true,
                'attr'=> [
                ]
            ])
            ->add('email',EmailType::class,[
                'required'=> true,
                'attr'=> [

                ]
            ])
            ->add('message',TextareaType::class,[
                'label' => 'Message',
                'attr'=>[
                    'rows'=> 0,
                    'cols'=>0,
                ]
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Contact::class,
        ]);
    }
}
