<?php

namespace App\Repository;

use App\Data\SearchData;
use App\Entity\Candidat;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\QueryBuilder;
use Doctrine\Persistence\ManagerRegistry;
use Knp\Component\Pager\Paginator;
use Knp\Component\Pager\PaginatorInterface;

/**
 * @method Candidat|null find($id, $lockMode = null, $lockVersion = null)
 * @method Candidat|null findOneBy(array $criteria, array $orderBy = null)
 * @method Candidat[]    findAll()
 * @method Candidat[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class CandidatRepository extends ServiceEntityRepository
{
    private $paginator;
    public function __construct(ManagerRegistry $registry,PaginatorInterface $paginator)
    {
        parent::__construct($registry, Candidat::class);
        $this->paginator = $paginator;
    }
    public function find5()
    {

        return $this->createQueryBuilder('c')
                    ->select('c','a')
                    ->join('c.avatar','a')
                    ->where('c.niveau is not null ')
                    ->andWhere('c.metier is not null')
                    // ->andWhere('c.pays is not null ' )
                    ->setMaxResults('5')
                    ->orderBy('c.id','DESC')
                    ->getQuery()
                    ->getResult();

    }

 
     /**
     * @Return PaginatorInterface
     */
    public function findsearch(SearchData $searchData): \Knp\Component\Pager\Pagination\PaginationInterface
    {
        $query =  $this->getsearchData($searchData)->getQuery();
            
        return $this->paginator->paginate(
            $query,
            $searchData->page,
            20
        );

            ;
    }
    public function getsearchData(SearchData $searchData, $ignoresalaire = false ):QueryBuilder
    {
        $query = $this->createQueryBuilder('c')
                ->select('c','a')
                ->join('c.avatar','a')
                 ->where('c.niveau is not null ')
                 ->andWhere('c.metier is not null');
                // ->andWhere('c.pays is not null ' );

         if (!empty($searchData->q))
         {
            $query = $query
                 ->andWhere('c.metier LIKE :q')
                 ->orWhere('c.nom LIKE :q')
                 ->orWhere('c.prenom LIKE :q')
                ->setParameter('q', "%{$searchData->q}%");
               
         }
         if (!empty($searchData->pays))
         {
            $query = $query
                 ->andWhere('c.pays LIKE :pays')
                ->setParameter('pays', "%{$searchData->pays}%");
         }
         if (!empty($searchData->etud))
         {
                 $query = $query->andWhere('c.niveau =  :etud')
                     ->setParameter('etud',"{$searchData->etud}");
         }
         if(!empty($searchData->exp))
         {
             $query = $query->andWhere('c.anexp = :exp')
                        ->setParameter('exp',"{$searchData->exp}");
         }

         return $query ;

    }

    /*
    public function findOneBySomeField($value): ?Candidat
    {
        return $this->createQueryBuilder('c')
            ->andWhere('c.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
