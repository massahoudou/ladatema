<?php

namespace App\Repository;

use App\Entity\Motdirecteur;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method Motdirecteur|null find($id, $lockMode = null, $lockVersion = null)
 * @method Motdirecteur|null findOneBy(array $criteria, array $orderBy = null)
 * @method Motdirecteur[]    findAll()
 * @method Motdirecteur[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class MotdirecteurRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Motdirecteur::class);
    }
    public function findOne()
    {
      return $this->createQueryBuilder('m')
          ->orderBy('m.id', 'DESC')
          ->setMaxResults(1)
          ->getQuery()
          ->getResult()
          ;
    }

    // /**
    //  * @return Motdirecteur[] Returns an array of Motdirecteur objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('m')
            ->andWhere('m.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('m.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?Motdirecteur
    {
        return $this->createQueryBuilder('m')
            ->andWhere('m.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
