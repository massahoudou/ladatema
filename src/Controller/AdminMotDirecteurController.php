<?php

namespace App\Controller;

use App\Entity\Motdirecteur;
use App\Form\MotdirecteurType;
use App\Repository\MotdirecteurRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/admin")
 */
class AdminMotDirecteurController extends AbstractController
{
    /**
     * @Route("_directeur", name="admin_mot_directeur_index", methods={"GET","POST"})
     */
    public function index(MotdirecteurRepository $motdirecteurRepository,Request $request): Response
    {

      $motdirecteur = new Motdirecteur();
      $form = $this->createForm(MotdirecteurType::class, $motdirecteur);
      $form->handleRequest($request);

      if ($form->isSubmitted() && $form->isValid()) {

          $entityManager = $this->getDoctrine()->getManager();
          $motdirecteur->setDateAt( new \DateTime('now'));
          $entityManager->persist($motdirecteur);
          $entityManager->flush();
          $this->addFlash('mot_success','Enregistrer avec succes');

          return $this->redirectToRoute('admin_mot_directeur_index');
      }

        return $this->render('admin/mot_directeur/index.html.twig', [
            'motdirecteurs' => $motdirecteurRepository->findAll(),
              'motdirecteur' => $motdirecteur,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("_directeur_{id}", name="admin_mot_directeur_update", methods={"GET","POST"})
     */
    public function new($id,Request $request , MotdirecteurRepository $motdirecteurRepository): Response
    {
        $motdirecteur = $motdirecteurRepository->findOneBy(['id'=> $id]);
        $form = $this->createForm(MotdirecteurType::class, $motdirecteur);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager = $this->getDoctrine()->getManager();
            $motdirecteur->setDateAt( new \DateTime('now'));
            $entityManager->persist($motdirecteur);
            $entityManager->flush();
            $this->addFlash('mot_update','Modifier avec succes');
            return $this->redirectToRoute('admin_mot_directeur_index');
        }

        return $this->render('admin/mot_directeur/index.html.twig', [
            'motdirecteur' => $motdirecteur,
            'motdirecteurs' => $motdirecteurRepository->findAll(),
              'form' => $form->createView(),

        ]);
    }

    // /**
    //  * @Route("_{id}", name="admin_mot_directeur_show", methods={"GET"})
    //  */
    // public function show(Motdirecteur $motdirecteur): Response
    // {
    //     return $this->render('admin/mot_directeur/show.html.twig', [
    //         'motdirecteur' => $motdirecteur,
    //     ]);
    // }

    // /**
    //  * @Route("_directeur_edit", name="admin_mot_directeur_edit", methods={"GET","POST"})
    //  */
    // public function edit($id,Request $request,MotdirecteurRepository $motdirecteurRepository): Response
    // {
    //   dd($id);
    //     $motdirecteur = new Motdirecteur();
    //      $motdirecteur = $motdirecteurRepository->findOneBy(['id' => $id ]);
    //     $form = $this->createForm(MotdirecteurType::class, $motdirecteur);
    //     $form->handleRequest($request);
    //
    //     if ($form->isSubmitted() && $form->isValid()) {
    //         $this->getDoctrine()->getManager()->flush();
    //         $this->addFlash('mot_update','Enregistrer avec succes');
    //
    //         return $this->redirectToRoute('admin_mot_directeur_index');
    //
    //     }

    //     return $this->render('admin/mot_directeur/edit.html.twig', [
    //       'motdirecteurs' => $motdirecteurRepository->findAll(),
    //         'motdirecteur' => $motdirecteur,
    //         'form' => $form->createView(),
    //     ]);
    // }

    /**
     * @Route("/directeur_delete_{id}", name="admin_mot_directeur_delete", methods={"POST"})
     */
    public function delete(Request $request, Motdirecteur $motdirecteur): Response
    {
        if ($this->isCsrfTokenValid('delete'.$motdirecteur->getId(), $request->request->get('_token'))) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->remove($motdirecteur);
            $entityManager->flush();
            $this->addFlash('mot_delete','Supprimer avec succes');
        }

        return $this->redirectToRoute('admin_mot_directeur_index');
    }
}
