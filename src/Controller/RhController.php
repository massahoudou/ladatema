<?php


namespace App\Controller;

use App\Data\SearchData;
use App\Entity\Candidat;
use App\Entity\Offre;
use App\Entity\Secteur;
use App\Form\SearchType;
use App\Form\SearchType2;
use App\Form\SearchTypeCandidatFormType;
use App\Repository\CandidatRepository;
use App\Repository\OffreRepository;
use App\Repository\RecruteurRepository;
use App\Repository\SecteurRepository;
use Doctrine\ORM\EntityManagerInterface;
use FOS\CKEditorBundle\Form\Type\CKEditorType;
use PhpParser\Node\Expr\Cast\String_;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Stringable;
use Symfony\Bridge\Twig\Mime\TemplatedEmail;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\HttpFoundation\File\Exception\FileException;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Mailer\MailerInterface;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\String\Slugger\SluggerInterface;
use Symfony\Component\Validator\Constraints\All;
use Symfony\Component\Validator\Constraints\File;
use Symfony\Component\Validator\Constraints\Regex;

use function PHPSTORM_META\map;

/**
 * Class RhController
 * @package App\Controller
 */
class RhController extends AbstractController
{
    private $repos;
    public function __construct(OffreRepository $offreRepository)
    {
        $this->repos = $offreRepository;
    }

    // public function migratuserid()
    // {
    //     $offres  = array ($this->repos->findAll());
    //     $lenght = count($offres) ;


    //     foreach ($offres as $offre => $value) {
    //         if($offre->getAdmin() !== null  )
    //         {
    //            $user =  $offre->getAdmin();
    //             if($offre->getUser !== null )
    //              $offre->setUser($user);
    //         }
    //         elseif($offre->getRecruteur() !== null )
    //         {
    //             $user = $offre->getRecruteur();
    //             if($offre->getUser !== null )
    //             $offre->setUser($user);
    //         }
    //     }



    // }


    /**
     * @Route("/rh" , name="rh_home")
     * @param SecteurRepository $secteurRepository
     * @param RecruteurRepository $recruteurRepository
     * @param Request $request
     * @return Response
     */
    public function index(
        SecteurRepository  $secteurRepository,
        RecruteurRepository $recruteurRepository,
        Request $request,
        CandidatRepository  $candidatRepository,
        OffreRepository $repository,
        EntityManagerInterface $manager
    ) {
        // dd($this->repos->findExpired());

        $data = new SearchData();
        $form2 = $this->createForm(SearchType2::class, $data);
        $form2->handleRequest($request);
        if ($form2->isSubmitted() && $form2->isValid()) {
            return $this->redirectToRoute('rh_offre', [
                'q' => $data->q,
                'pays' => $data->pays,
                'exp' => $data->exp,
                'etud' => $data->etud,
            ]);
        }



        return $this->render('rh/index.html.twig', [
            'controller_name' => 'Rh',
            'form' => $form2->createView(),
            'offres' => $this->repos->find10(),
            'secteurs' => $secteurRepository->findAll(),
            'recruteurs' => $recruteurRepository->findRecruteur(),
            'candidats' => $candidatRepository->find5(),
            'countoffre' => $repository->counter(),
            'countentreprise' => $recruteurRepository->counter(),
            'candidats' => $candidatRepository->find5(),
            //'count' => $secteurRepository->count2()
        ]);
    }

    /**
     * @return Response@
     * @Route("/offre" , name="rh_offre")
     *
     */
    public function offre(Request $request, SecteurRepository $repository)
    {
        $data = new SearchData();
        $form = $this->createForm(SearchType::class, $data);
        $form->handleRequest($request);


        if ($request->get('secteur')) {
            $secteur = $repository->findOneBy(['id' => $request->get('id')]);
            // dd($secteur);
            $request->request->set('secteur', $secteur);

            $data->secteur  = $request->get('secteur');
        } else {
            $data->secteur  = $request->get('secteur');
        }

        // dd($data);
        $data->pays = $request->get('pays');
        $data->q = $request->get('q');
        $data->page = $request->get('page', 1);

        // [$min, $max] = $this->repos->findminmax($data);

        // dd($form);
        return $this->render('rh/offre/candidat.html.twig', [
            'Controller_Name' => 'offre d\'emploi',
            'form' => $form->createView(),
            'offres' => $this->repos->findSearch($data),
            // 'min' => $min,
            // 'max' => $max,
        ]);
    }


    /**
     * @Route("/offre",name="rh_offre2")
     * @param $secteur[]
     * @param Request $request
     * @param SecteurRepository $repository
     * @return Response
     */
    public function offreParSecteur($secteur, Request $request, SecteurRepository $repository)
    {
        $data = new SearchData();
        $data->page = $request->get('page', 1);
        // [$min, $max] = $this->repos->findminmax($data);
        $form = $this->createForm(SearchType::class, $data);

        $secteur =  $repository->findOneBy(['id' => $secteur]);
        $request->request->set('secteur', $secteur);
        $data->secteur  = $request->get('secteur');
        $form->handleRequest($request);
        return $this->render('rh/offre/candidat.html.twig', [
            'Controller_Name' => 'offre d\'emploi',
            'form' => $form->createView(),
            'offres' => $this->repos->findSearch($data),
            // 'min' => $min,
            // 'max' => $max,
        ]);
    }


    /**
     * @return Response
     * @Route("/cvthèque" , name="rh_cv" )
     */
    public function cvtheque(Request $request, CandidatRepository $repository)
    {

        $data = new SearchData();
        $data->pays = $request->get('pays');
        $data->q = $request->get('q');
        $data->page = $request->get('page', 1);
        // [$min, $max] = $this->repos->findminmax($data);
        $form = $this->createForm(SearchTypeCandidatFormType::class, $data);

        $form->handleRequest($request);

        return $this->render('rh/cv/cvtheque.html.twig', [
            'Controller_Name' => 'cvthèque',
            'form' => $form->createView(),
            'candidats' => $repository->findSearch($data),
            // 'min' => $min,
            // 'max' => $max,
        ]);
    }
    /**
     * @return Response
     * @Route("/candidat" , name="rh_candidat")
     */
    public function candidat()
    {

        return $this->render('rh/offre/candidat.html.twig', [
            'Controller_Name' => 'cvthèque',
            'offres' => $this->repos->findAll(),

        ]);
    }

    /**
     * @Route("/offre=_{titre}_{id}",name="une_offre")
     * @param $id
     * @return Response
     */
    public function show($titre, $id, Request $request, SecteurRepository $secteurRepository, SluggerInterface $slugger, MailerInterface $mailerInterface): Response
    {

        $fromBuilder = $this->createFormBuilder()->add('object', TextType::class, [
            'attr' => ['value' => ' candidature au poste de '.$titre ],
            'constraints' => [
                new Regex("/[^0-9#&_+@=:,$(\/[\])]+$/A","Entré non valide !"),
            ]
            ])
            ->add('message', TextareaType::class, [
                'attr' => ['placeholder' => 'Votre message '],
                'constraints' => [
                    new Regex("/[^#&_+@=$\/[\]]+$/A","Entré non valide !"),
                ]
                ])
            ->add('file',FileType::class, [
                'multiple' => true,
                'constraints' => [
                    new All([
                        'constraints' => [
                            new File([
                                'maxSize' => '1024k',
                                'mimeTypes' => [
                                    'application/pdf',
                                    'application/x-pdf',
                                ],
                                'mimeTypesMessage' => 'Svp un document valide ! :) ',
                            ])
                        ]
                    ])
                ],

            ])->getForm();
        $fromBuilder->handleRequest($request);
        $une_offre = $this->repos->findOneBy(['id' => $id]);
        $allfile = [] ;
        if ($fromBuilder->isSubmitted() && $fromBuilder->isValid()) {
            $files = $fromBuilder->get('file')->getData();
            
            
            if (!empty($files)) {
                foreach ($files as $file) {
                    $originalFilename = pathinfo($file->getClientOriginalName(), PATHINFO_FILENAME);
                    // this is needed to safely include the file name as part of the URL
                    $safeFilename = $slugger->slug($originalFilename);
                    $newFilename = $safeFilename . '-' . uniqid() . '.' . $file->guessExtension();
                    try {
                        $file->move(
                            $this->getParameter('file_directory'),
                            $newFilename
                        );
                    } catch (FileException $e) {
                        dd($e);
                    }
                  $allfile [] = $newFilename;
                }
                // dd($allfile);
                $object = $fromBuilder->get('object')->getData();
                $message = $fromBuilder->get('message')->getData() ;
                $email = (new TemplatedEmail())
                    ->from('newsletter@site.fr')
                    ->to($une_offre->getRecruteur() != null ? $une_offre->getRecruteur()->getEmail() : "admin@gmail.com")
                    ->subject('Candidature au poste ' . strval($une_offre->getTitre()))
                    ->htmlTemplate("components/_postuleEmails.html.twig")
                    ->context(compact('allfile', 'object','message'));
                $mailerInterface->send($email);
            }

            return $this->redirectToRoute("une_offre", [
                'titre' => $une_offre->getTitre(),
                'id' => $une_offre->getId(),
            ]);
        }

        $secteur = $secteurRepository->SearchSecteur($id);
        $offre_relatif = $this->repos->findRelatif($secteur);
        $offre = $this->repos->find10();
        return $this->render('rh/voir.html.twig', [
            'controllerName' => 'Une offre',
            'une_offre' => $une_offre,
            'offres' => $offre,
            'form' => $fromBuilder->createView(),
            'offre_rel' => $offre_relatif,
        ]);
    }
    /**
     * @Route("/cv_{email}", name="un_cv")
     */
    public function showcv($email, CandidatRepository  $candidatRepository)
    {
        $offre = $this->repos->OffreRecent();
        $un_candidat = $candidatRepository->findOneBy(['email' => $email]);
        return $this->render('rh/voircv.html.twig', [
            'ControllerName' => 'Un  cv',
            'un_candidat' => $un_candidat,
            'offres' => $offre,
        ]);
    }
    /**
     * @Route("/postule", name="app.postulation")
     */
    public function Postulation(Request $request)
    {
        $file = $request->request->get('file');
        dd($file);



        // if( $this->getUser() == true )
        // {
        //     $fromBuilder = $this->createFormBuilder()->add('titre',TextType::class,['attr' => ['value' => 'Candidature']])
        //     ->add('lettre',TextareaType::class,['attr' => ['placeholder' => 'Votre  lettre de candidature...']]);
        // }else{
        //     $fromBuilder = $this->createFormBuilder()->add('nom',TextType::class,['attr' => ['value' => 'Candidature']])
        //     ->add('lettre',TextareaType::class,['attr' => ['placeholder' => 'Votre  lettre de candidature...']])
        //     ->add('fichier',FileType::class,[
        //         'multiple' => true ,
        //         'label' => 'Pièce jointe '
        //     ]);
        // }



        return $this->redirectToRoute('une_offre', [
            'titre' =>  'un titre',
            'id' => 'identifiant'
        ]);;
    }
}
