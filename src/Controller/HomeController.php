<?php

namespace App\Controller;


use App\Entity\Email;
use App\Form\EmailCollecteType;
use App\Repository\AdminRepository;
use App\Repository\AproposRepository;
use App\Repository\AvisRepository;
use App\Repository\CandidatRepository;
use App\Repository\OffreRepository;
use App\Repository\RecruteurRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;
use App\Repository\PartenaireRepository;
use App\Form\NewslettersType;
use App\Repository\UserRepository;
use Symfony\Component\HttpFoundation\Request;

class HomeController extends AbstractController
{
    /**
     * @Route("/", name="home")
     */
    public function index(OffreRepository $repository,
    AvisRepository $avisRepository,
    AproposRepository $aproposRepository,
    RecruteurRepository  $recruteurRepository,
     AdminRepository $adminRepository,
     EntityManagerInterface $entityManagerInterface,
     CandidatRepository $candidatRepository,
     UserPasswordEncoderInterface $encoder,
     PartenaireRepository $partenaireRepository,
     Request $request,
     EntityManagerInterface $manager ): Response
    {
        // $mail ='admin@gmail.com';
        // $admin = $adminRepository->findOneBy(['email'=> $mail ]);
        //
        //  if (!$admin)
        //  {
        //      $adminentiti = new Admin();
        //     $adminentiti->setEmail($mail)->setPassword($encoder->encodePassword($adminentiti,'123456'))->setRoles(["ROLE_ADMIN"]);
        //     $entityManagerInterface->persist($adminentiti);
        //     $entityManagerInterface->flush();
        // }
        $user_email = new Email();
        $new_form = $this->createForm(EmailCollecteType::class,$user_email);
        $new_form->handleRequest($request);
        
        if($new_form->isSubmitted() && $new_form->isValid() )
        {   
            $manager->persist($user_email);
            $manager->flush();
            $this->addFlash('newsletter' , 'Votre Abonement a été effectuer avec success ');
            
            return  $this->redirectToRoute('home');
        }
        return $this->render('home/index.html.twig', [
            'controller_name' => 'Ladatem Research',
            'news_form' => $new_form->createView(),  
            'offres' =>  $repository->find10(),
            'avis' => $avisRepository->avisthree(),
            'aproposfinance' => $aproposRepository->cocherfinnace(),
            'aproposrh' => $aproposRepository->cocherRh(),
            'countoffre' => $repository->counter(),
            'countentreprise' => $recruteurRepository->counter(),
            'candidats' => $candidatRepository->find5(),
            'partenaires'=> $partenaireRepository->findAll(),
        ]);
    }

    /**
     * @Route("/token")
     */
    public function tokenGenerator (UserRepository $userRepository ,EntityManagerInterface $manager) {

        $users = $userRepository->findAll(); 

        foreach($users as $user) {
          
            if($user->getValidationToken() == '' || $user->getValidationToken() == null) {
               
                $token = hash('sha256', uniqid());
                $user->setValidationToken($token);
                $manager->persist($user);
                $manager->flush() ; 
                dump("fait");
            }else {
                break ;
            }
        }

        return $this->redirectToRoute("login") ;
    }
}
