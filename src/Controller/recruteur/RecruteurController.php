<?php 

namespace App\Controller\recruteur;

use App\Entity\Offre;
use App\Entity\Recruteur;
use App\Form\OffreRecruteurFormType;
use App\Form\OffreType;
use App\Form\RecruteurFinalType;
use App\Form\RecruteurFormType;
use App\Repository\OffreRepository;
use App\Repository\RecruteurRepository;
use App\Repository\UserRepository;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\EntityManagerInterface;
use phpDocumentor\Reflection\Types\This;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Form\Extension\Core\Type\RepeatedType;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Validator\Constraints\Length;
use Symfony\Component\Validator\Constraints\NotBlank;

/**
 * @Route("/profile")
 */
class RecruteurController extends AbstractController 
{
    private $repos;
    /**
     * @var EntityManagerInterface
     */
    private $manager;

    public function __construct(RecruteurRepository $recruteurRepository,EntityManagerInterface  $manager)
    {
        $this->repos = $recruteurRepository;
        $this->manager = $manager;
    }

    /**
     * @Route("/recruteur",name="recruteur")
     */
    public function index(UserInterface  $user,OffreRepository $repository)
    {

        $offres = $repository->findOffreRecruteur($user);
         return $this->render('user/recruteur/index.html.twig',[
            'controllerName' => 'Profile',
             'offres'=> $offres,
        ]);
    }

    /**
     * @Route("/recruteur_offre",name="recru_offre")
     * @param $id
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function offre( Request $request,UserInterface $user)
    {
        $offre = new Offre();
         $recruteur = $this->repos->findoneBY(['email'=> $user->getUsername()]);
    
        $form = $this->createForm(OffreRecruteurFormType::class,$offre);
        $form->handleRequest($request);
        
        if ($form->isSubmitted() && $form->isValid()) 
        { 
            $offre->setRecruteur($recruteur);
            $offre->setArchive(true);
            $offre->setUpdateAt(new \DateTime('now'));
            $date = new \DateTime('now') ;
            if($offre->getDelai()->format("Y-m-d") > $date->format("Y-m-d") )
            {
                $offre->setIsExpired(false);
            }
            $this->manager->persist($offre);
            $this->manager->flush();
            $this->addFlash('recruteur_offre','Bien enregistrer');
           return  $this->redirectToRoute('recruteur');
        }
        return $this->render('user/recruteur/offre.html.twig',[
            'controllerName' => 'Profile',
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/recruteur_edition{id}",name="edtion")
     * @param $id
     * @param Offre $offre
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function edition($id,Request $request,OffreRepository $repository)
    {
        $offre = new Offre();
        $offre = $repository->findOneBy(['id'=> $id ]);
        $form = $this->createForm(OffreRecruteurFormType::class,$offre);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid())
        {
            $offre->setUpdateAt(new \DateTime('now'));
            $date = new \DateTime('now') ;
            if($offre->getDelai()->format("Y-m-d") > $date->format("Y-m-d") )
            {
                $offre->setIsExpired(false);
            }
            $this->manager->persist($offre);
            $this->manager->flush();
            $this->addFlash('edition', 'Modifier avec success');
           return $this->redirectToRoute('recruteur');
        }
        return $this->render('user/recruteur/edit.html.twig',[
            'form'=> $form->createView(),
        ]);
    }

    /**
     * @Route("/recruteur_delete{id}",name="recruteur_delete")
     */
    public function delete($id,OffreRepository $repository)
    {
        $offre = $repository->findOneBy(['id'=> $id]);
        $this->manager->remove($offre);
        $this->manager->flush();
        $this->addFlash('delete' ,'Offre suppirmer avec sucess');
       return  $this->redirectToRoute('recruteur');
    }

    /**
     * @Route("/infoModi",name="modification")
     * @param UserInterface $user
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function RecruteurModfi(UserInterface $user,Request $request)
    {
        $recruteur = $this->repos->findOneBy(['email' => $user->getUsername()]);
        $form = $this->createForm(RecruteurFinalType::class,$recruteur);
        $form->handleRequest($request );

        if ($form->isSubmitted() && $form->isValid())
        {
            $this->manager->persist($recruteur);
            $this->manager->flush();
            $this->addFlash('profile','Modifier avec success');
           return  $this->redirectToRoute('recruteur');
        }
        return $this->render('user/recruteur/infomodif.html.twig',[
            'form' => $form->createView(),
        ]);
    }
    /**
     * @Route("/password", name="password.change")
     */
    public function changerDeMotPasse(UserRepository $userRepository,Request $request,EntityManagerInterface $manager,UserPasswordEncoderInterface  $encoder) {
        
        $user = $userRepository->findOneBy(['email'=> $this->getUser()->getUserIdentifier() ]); 
        if( $this->getUser() == true )
        {
            $fromBuilder = $this->createFormBuilder()->add('plainPassword', RepeatedType::class, [
                // instead of being set onto the object directly,
                // this is read and encoded in the controller
                'type' => PasswordType::class,
                'invalid_message' => 'Les mots de passe doivent conrespondre.',
                'options' => ['attr' => ['class' => 'password-field']],
                'required' => true,
                'first_options'  => ['label' => 'Mot de passe'],
                'second_options' => ['label' => 'Confirmé le mot de passe'],
                'mapped' => false,
                'constraints' => [
                    new NotBlank([
                        'message' => 'Entrer un mot de passe ',
                    ]),
                    new Length([
                        'min' => 6,
                        'minMessage' => 'votr mot de passe doit est etre qu moins de  {{ limit }} caratères',
                        // max length allowed by Symfony for security reasons
                        'max' => 4096,
                    ]),
                ],
            ])->getForm() ;
            $fromBuilder->handleRequest($request);

            if($fromBuilder->isSubmitted() && $fromBuilder->isValid())
            {
            
                 $user->setPassword($encoder->encodePassword($user,$fromBuilder->get('plainPassword')->getData()));
                // dd($user);
                $manager->persist($user);
                $manager->flush();
                $this->addFlash('cp','mot passe changer avec success ') ;
                return $this->redirectToRoute('logout');
            }

            return $this->render('user/changePassword.html.twig',[
                'form'=> $fromBuilder->createView(),
        ]);
            
        }
        return $this->redirectToRoute('logout');
      
    }

}
