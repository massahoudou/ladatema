<?php

namespace App\Controller;


use App\Entity\Partenaire;
use App\Form\PartenaireType;
use App\Repository\PartenaireRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

class AdminPartenaireController extends AbstractController
{
    /**
     * @var EntityManagerInterface
     */
    private $manager;
    /**
     * @var PartenaireRepository
     */
    private $repos;

    public function __Construct(EntityManagerInterface $manager, PartenaireRepository  $repository){
        $this->manager = $manager;
        $this->repos = $repository;
    }

    /**
     * @Route("/admin_partenaire" , name="admin_partenaire")
     * @param Request $request
     * @param PartenaireRepository $repository
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function index (Request $request)
    {
        $partenaire  = new Partenaire();
        $form = $this->createForm(PartenaireType::class, $partenaire);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid())
        {

             $this->manager->persist($partenaire);
            $this->manager->flush();
            $this->addFlash('partenaire_new','Creer avec success');
            $this->redirectToRoute('admin_partenaire');
        }

        return $this->render('admin/partenaire/index.html.twig',[
            'form' => $form->createView(),
            'partenaire' =>   $this->repos->findAll(),
        ]);
    }

    /**
     * @Route("/admin_partenaire_update{id}",name="admin_partenaire_update")
     * @param $id
     * @param Request $request
     * @param PartenaireRepository $repository
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function update($id,Request $request,PartenaireRepository  $repository)
    {
        $partenaire = $repository->findOneBy(['id' => $id] );
        $form = $this->createForm(PartenaireType::class,$partenaire);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid())
        {
            $this->manager->persist($partenaire);
            $this->manager->flush();
            $this->addFlash('partenaire_update','modifier avec success');
            $this->redirectToRoute('admin_partenaire');
        }
        return $this->render('admin/partenaire/index.html.twig',[
            'form' => $form->createView(),
            'partenaire' =>   $repository->findAll(),
        ]);
    }

    /**
     * @Route("/admin_partenaire_delete{id}" , name="admin_partenaire_delete", methods={"POST"})
     */
    public function delete($id ,Request $request)
    {
         $partenaire = $this->repos->findOneBy(['id' => $id] );
         if($partenaire )
         {
           if($this->isCsrfTokenValid('delete'.$id , $request->request->get('_token') ))
           {
             $this->manager->remove($partenaire);
             $this->manager->flush();
             $this->addFlash('partenaire_delete','supprimer avec success');
            return  $this->redirectToRoute('admin_partenaire');
           }
            return  $this->redirectToRoute('admin_partenaire');
         }

        $this->addFlash('partenaire_delete','Erreur de suppression ');
       return  $this->redirectToRoute('admin_partenaire');


    }
}
