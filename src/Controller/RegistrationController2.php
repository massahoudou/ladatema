<?php

namespace App\Controller;

use App\Entity\Admin;
use App\Entity\Candidat;
use App\Entity\Recruteur;
use App\Entity\Users;
use App\Form\CandidatFormType;
use App\Form\RecruteurFormType;
use App\Form\RegistrationFormType;
use App\Repository\UserRepository;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\SqlFormatter\Token;
use Symfony\Bridge\Twig\Mime\TemplatedEmail;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Mailer\Mailer;
use Symfony\Component\Mailer\MailerInterface;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

class RegistrationController2 extends AbstractController
{
    private $passwordEncoder;

    private $manager;
    public function __construct(EntityManagerInterface $manager, UserPasswordEncoderInterface $passwordEncoder)
    {
        $this->passwordEncoder = $passwordEncoder;
        $this->manager = $manager;
    }
    /**
     * @Route("/admin/register", name="app_register")
     */
    public function Adminregister(Request $request): Response
    {
        $user = new Admin();
        $form = $this->createForm(RegistrationFormType::class, $user);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $token = hash('sha256', uniqid());
            $user->setValidationToken($token);
            $user->setPassword(
                $this->passwordEncoder->encodePassword(
                    $user,
                    $form->get('plainPassword')->getData()
                )
            );


            $this->manager->persist($user);
            $this->manager->flush();
            // do anything else you need here, like send an email

            return $this->redirectToRoute('rh_offre');
        }

        return $this->render('registration2/register.html.twig', [
            'registrationForm' => $form->createView(),
        ]);
    }
    /**
     * @Route("/inscription",name="inscription")
     */
    public function inscription()
    {
        return $this->render('registration2/inscription.html.twig', [
            'ControllerName' => 'inscription',
        ]);
    }
    /**
     * @Route("/inscription/candidat", name="app_register_candidat")
     */
    public function candidatRegister(Request $request, MailerInterface $mailerInterface)
    {
        $candidat = new Candidat();
        $form = $this->createForm(CandidatFormType::class, $candidat);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {

            $token = hash('sha256', uniqid());
            $candidat->setValidationToken($token);
            $candidat->setPassword(
                $this->passwordEncoder->encodePassword(
                    $candidat,
                    $form->get('plainPassword')->getData()
                )
            );
            $candidat->setRoles(['ROLE_USER', 'ROLE_CAN']);
            $candidat->setValidationToken($token);
            $user = $candidat ; 
            $candidat->setIsValid(false);
            $this->manager->persist($candidat);
            $this->manager->flush();
            $email = (new TemplatedEmail())
                ->from('newsletter@site.fr')
                ->to($candidat->getEmail())
                ->subject('Activation du compte ')
                ->htmlTemplate("components/_email.html.twig")
                ->context(compact('user', 'token'));
            $mailerInterface->send($email);
            $this->addFlash('loginemail', 'un mail a été envoyer sur votre adresse consulter le pour achever l\'inscription  ');
            // do anything else you need here, like send an email

            return $this->redirectToRoute('login');
        }

        return $this->render('registration2/candidat.html.twig', [
            'form' => $form->createView(),
        ]);
    }
    /**
     * @Route("/inscription/recruteur", name="app_register_recruteur")
     */
    public function recruteurRegister(Request $request,MailerInterface $mailerInterface)
    {
        $recruteur = new Recruteur();
        $form = $this->createForm(RecruteurFormType::class, $recruteur);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $token = hash('sha256', uniqid());
            $recruteur->setValidationToken($token);
            $recruteur->setPassword(
                $this->passwordEncoder->encodePassword(
                    $recruteur,
                    $form->get('plainPassword')->getData()
                )
            );
            $recruteur->setRoles(['ROLE_USER', 'ROLE_REC']);
            $recruteur->setValidationToken($token);
            $recruteur->setIsValid(false);
            $user = $recruteur;
            $manager2 = $this->getDoctrine()->getManager();
            $manager2->persist($recruteur);
            $manager2->flush();
            $email = (new TemplatedEmail())
                ->from('newsletter@site.fr')
                ->to($recruteur->getEmail())
                ->subject('Activation du comtpe ')
                ->htmlTemplate("components/_email.html.twig")
                ->context(compact('user', 'token'));
            $mailerInterface->send($email);
            $this->addFlash('loginemail', 'un mail a été envoyer sur votre adresse consulter le pour achever l\'inscription  ');
            // do anything else you need here, like send an email

            return $this->redirectToRoute('login');
        }

        return $this->render('registration2/recruteur.html.twig', [
            'form' => $form->createView(),
        ]);
    }
    /**
     * @Route("/inscription/{token}" , name="app_register_confirm")
     */
    public function confirm($token,UserRepository $userRepository)
    {
        $user = $userRepository->findOneBy(['validation_token' => $token ]);
        if ($user->getValidationToken() != $token) {
            throw $this->createNotFoundException('Page non trouver ');
        }
        $user->setIsValid(true);
        $this->manager->persist($user);
        $this->manager->flush();
        $this->addFlash('compte-activer', 'Votre à été activé  ');
   
        return  $this->redirectToRoute('login');
    }
}
