<?php

namespace App\Controller;

use App\Entity\Users;
use App\Form\NewslettersType;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class NewsletterController extends AbstractController
{
    /**
     * @Route("/newsletter", name="app_newsletter")
     */
    public function index(): Response
    {
        
        return $this->render('newsletter/index.html.twig', [
           
        ]);
    }
}
