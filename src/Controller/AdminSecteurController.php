<?php 

namespace App\Controller;

use App\Entity\Secteur;
use App\Form\SecteurType;
use App\Repository\SecteurRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

class AdminSecteurController  extends AbstractController
{
    private $repos;
    private $manager;

    public function __construct(SecteurRepository $repos,EntityManagerInterface $entityManager)
    {   
        $this->repos = $repos;
        $this->manager = $entityManager;
        
    }

    /**
     * @Route("/admin_secteur",name="secteur")
     */
    public function index()
    {
        $secteur = new Secteur();
        $form = $this->createForm(SecteurType::class , $secteur);
        return $this->render('admin/secteur/index.html.twig',[
            'form' => $form->createView(),
            'secteurs' => $this->repos->findAll()
        ]);
    }
    /**
     * @Route("/admin_secteur_new",name="secteur_new")
     */
    public function new(Request $request)
    {
        $secteur = new Secteur();
        
        $form = $this->createForm(SecteurType::class , $secteur);
        $form->handleRequest($request);

        if($form->isSubmitted() && $form->isValid())
        {
            $this->manager->persist($secteur);
            $this->manager->flush();
            $this->addFlash('secteur_new' , 'Creer  avec success');
            return $this->redirectToRoute('secteur_new');
        }
        return $this->render('admin/secteur/index.html.twig',[
                'form' => $form->createView(),
                'secteurs' => $this->repos->findAll()
            ]);
    }

    /**
     * @Route ("/admin_secteur_upadate{id}",name="secteur_update")
     * @param $id
     * @param Request $request
     * @return RedirectResponse
     */
    public function update($id,Request $request)
    {
        $secteur = $this->repos->findOneBy(['id' => $id ]);
        if ($secteur)
        {
            $form = $this->createForm(SecteurType::class,$secteur);
            $form->handleRequest($request);
            if ($form->isSubmitted() && $form->isValid())
            {
                $this->manager->persist($secteur);
                $this->manager->flush();
                $this->addFlash('secteur_modif','Modifier avec success');
                return $this->redirectToRoute('secteur_new') ;
            }
            return $this->render('admin/secteur/index.html.twig',[
                'form' => $form->createView(),
                'secteurs' => $this->repos->findAll()
            ]);
        }
        return $this->redirectToRoute('secteur_new') ;
    }

    /**
     * @Route("/admin_secteur_delete{id}",name="secteur_delete")
     * @param $id
     * @return RedirectResponse
     */
    public function delete($id)
    {
        $secteur = $this->repos->findOneBy(['id'=> $id ]);
        if($secteur)
        {
            $this->manager->remove($secteur);
            $this->manager->flush();
            return $this->redirectToRoute('secteur_new');
        }
        return $this->redirectToRoute('secteur_new');
    }
    

}