<?php


namespace App\Controller;


use App\Repository\ConseilRepository;
use App\Repository\DocumentRepository;
use Symfony\Component\Routing\Annotation\Route;

class DocConseilController extends \Symfony\Bundle\FrameworkBundle\Controller\AbstractController
{

    /**
     * @Route("/conseils" , name="conseils")
     * @param ConseilRepository $conseilRepository
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function conseil (ConseilRepository $conseilRepository)
    {
        return $this->render('rh/conseil.html.twig',[
            'conseils' => $conseilRepository->findAll()
        ]);
    }

    /**
     * @Route("/documents" ,name="documents")
     * @param DocumentRepository $documentRepository
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function document (DocumentRepository  $documentRepository)
    {
        return $this->render('rh/doc.html.twig',[
            'docs' => $documentRepository->findAll(),
        ]);
    }
}