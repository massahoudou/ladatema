<?php

namespace App\Controller;

use App\Entity\Apropos;
use App\Entity\Candidat;
use App\Entity\Contact;
use App\Entity\Offre;
use App\Repository\AvisRepository;
use App\Repository\ContactRepository;
use App\Repository\OffreRepository;
use App\Repository\RecruteurRepository;
use App\Repository\ServiceRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Request;
use App\Repository\PartenaireRepository;


class AdminController  extends AbstractController
{

    public function addamdintooffre(EntityManagerInterface $manager)
    {
        $offres = $manager->getRepository(Offre::class)->findAll();

        foreach($offres as $offre)
        {
            $offre->setUsers($offre->getAdmin());
            $manager->persist($offre);
            $manager->flush();
        }
    }

    /**
     * @Route("/admin" ,name="admin")
     */
    public function index(EntityManagerInterface $manager, PartenaireRepository $partenaire,ServiceRepository $serviceRepository,ContactRepository $contactRepository,AvisRepository $avisRepository,OffreRepository $offreRepository,RecruteurRepository  $recruteurRepository)
    {
       
        $avis  =    $avisRepository->findAll();
        $offreattente = $offreRepository->findattente();
        $service = $serviceRepository->findAll();
        $contacts = $contactRepository->findAll();
        $candidats = $this->getDoctrine()->getRepository(Candidat::class)->findAll();
        $aprpos = $this->getDoctrine()->getRepository(Apropos::class)->findAll();
        return $this->render('admin/index.html.twig',[
            'avis' => $avis,
            'contacts' => $contacts,
            'service' => $service,
            'candidats' =>  $candidats,
            'offreattente' => $offreattente,
            'countoffre' => $offreRepository->counter(),
            'entreprise' => $recruteurRepository->counter(),
            'apropos' => $aprpos,
            'partenaire' => $partenaire->findAll(),
        ]);
    }

    /**
     * @Route("/admin_contact",name="admin_contact")
     * @param ContactRepository $contactRepository
     * @return Response
     */
    public function contact(ContactRepository $contactRepository)
    {
        $contact = $contactRepository->findAll();
        return $this->render('admin/contactans.html.twig',[
            'contacts' => $contact,
        ]);
    }

    /**
     * @Route("/admin_contact_delete_{id}" , name="admin_contact_delete" , methods={"POST"})
     * @return Response
     */
    public function delete($id ,Request $request ,EntityManagerInterface $manager,ContactRepository $contactRepository): Response
    {
        $contact = $contactRepository->findOneBy(['id'=>$id]);
        if($contact)
        {
          if ($this->isCsrfTokenValid('delete'.$contact->getId() , $request->request->get('_token'))){
            $manager->remove($contact);
            $manager->flush();
            $this->addFlash('contact_delete','Supprimer avec succes');
           return   $this->redirectToRoute('admin_contact');
          }
        }

        return $this->redirectToRoute('admin_contact');
    }
}
