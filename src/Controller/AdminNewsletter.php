<?php

namespace App\Controller ;

use App\Entity\Newsletter\Categories;
use App\Entity\Newsletter\Newsletter;
use App\Form\CategoriType;
use App\Form\NewsLetterType;
use App\Repository\Newsletter\CategoriesRepository;
use App\Repository\Newsletter\NewsletterRepository;
use App\Repository\UserRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bridge\Twig\Mime\TemplatedEmail;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Mailer\MailerInterface;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Validator\Constraints\Regex;

/**
 * @Route("/admin")
 */
class AdminNewsletter extends AbstractController
{

    private $manager ; 

    private $repos ;
    public function __construct(EntityManagerInterface $manager,NewsletterRepository $repos)
    {   
        $this->manager =  $manager ;
        $this->repos = $repos  ;
    }

    
    /**
     * @Route("_newsletter",name="admin_newsletter")
     */
    public function index(Request $request,UserRepository $repos)
    {
        $newsletter = new Newsletter();

        $form = $this->createForm(NewsLetterType::class,$newsletter);
        
        $form->handleRequest($request);
        $admin = $repos->findOneBy(['email'=> $this->getUser()->getUserIdentifier()]);
        if($form->isSubmitted() && $form->isValid())
        {       
            $newsletter->setUser($admin);
        
            $this->manager->persist($newsletter);
            $this->manager->flush();

             return $this->redirect('admin_newsletter');
        }
        
        return $this->render('admin/newsletter/index.html.twig',[
            'form' => $form->createView(),
            'newsletters' => $this->repos->findAll()
            
        ]);
    }

  


    /**
     * @Route("_send/{id}",name="admin_newsletter_send")
     */
    public function send(Newsletter $newsletter,MailerInterface $mailerInterface,EntityManagerInterface $manager)
    {
        $news = $newsletter ;
        $emails = $news->getCategories()->getEmails();

        
        foreach($emails as $user )
        {
            $email = (new TemplatedEmail())
                        ->from('newsletter@site.fr')
                        ->to($user->getEmail())
                        ->subject($news->getTitre())
                        ->htmlTemplate('components/_newsemail.html.twig')
                        ->context(compact('user','news'))
                    ;
            $mailerInterface->send($email);

        }
        $newsletter->setIsSent(true);
        $manager->persist($newsletter);
        $manager->flush();

        $this->addFlash(
           'email_success',
           'envoie terminer avec success !'
        );
        return $this->redirectToRoute('admin_newsletter');
    }

    /**
     * @Route("_newcategori", name="new.categoris")
     */
    public function newCategoris(Request $request,EntityManagerInterface $manager,CategoriesRepository $categories) {
        $categorie = new Categories();
        $form= $this->createForm(CategoriType::class,$categorie);
        $form->handleRequest($request);

        if($form->isSubmitted() && $form->isValid()) {
            $manager->persist($categorie);
            $manager->flush() ;
            $this->addFlash("categorie_new","Ajouter avec success") ;
           return  $this->redirectToRoute("new.categoris") ;
        }
        return $this->render('admin/newsletter/categorie.html.twig',[
            'categories' => $categories->findAll(),
            'form' => $form->createView(),
        ]);
    }
    /**
     * @Route("_editcategori_{id}", name="edit.categoris")
     */
    public function editCategoris(Categories $categorie,Request $request,EntityManagerInterface $manager,CategoriesRepository $categories) {
       $form = $this->createForm(CategoriType::class,$categorie);
       $form->handleRequest($request) ; 

       if($form->isSubmitted() && $form->isValid()) {
        $manager->persist($categorie);
        $manager->flush() ;
        $this->addFlash("categorie_new","Ajouter avec success") ;
         return  $this->redirectToRoute("new.categoris") ;
        }
        return $this->render('admin/newsletter/categorie.html.twig',[
            'categories' => $categories->findAll(),
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("_deletecategori_{id}", name="delete.categoris")
     */
    public function deleteCategoris($id, CategoriesRepository $categories,EntityManagerInterface $manager,Request $request) {
        $categorie = $categories->findOneBy(['id' => $id ]);
        if($categorie) {
            if($this->isCsrfTokenValid('delete'.$id , $request->request->get('_token') ))  {
            $manager->remove($categorie);
            $manager->flush();
            $this->addFlash("categorie_del","supprmier avec success") ;
            return $this->redirectToRoute("new.categoris") ;
           }
        }
        $this->addFlash("error","Erreur de suppresion") ;
        return $this->redirectToRoute("new.categoris") ;
    }
}