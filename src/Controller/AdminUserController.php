<?php


namespace App\Controller;


use App\Entity\Admin;
use App\Entity\Candidat;
use App\Form\AdminFormType;
use App\Repository\AdminRepository;
use App\Repository\CandidatRepository;
use App\Repository\RecruteurRepository;
use App\Repository\UserRepository;
use Doctrine\ORM\EntityManagerInterface;
use phpDocumentor\Reflection\Types\This;
use Symfony\Bridge\Twig\Mime\TemplatedEmail;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Form\Extension\Core\Type\RepeatedType;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Mailer\MailerInterface;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;
use Symfony\Component\Validator\Constraints\Length;
use Symfony\Component\Validator\Constraints\NotBlank;

/**
 * Class AdminUserController
 * @package App\Controller
 */
class AdminUserController extends AbstractController
{
    /**
     * @Route("/admin_user" , name="utilisateur")
     * @param UserRepository $userRepository
     * @return Response
     */
    public function index(AdminRepository $userRepository)
    {
        return $this->render('admin/user/utilisateur.html.twig', [
            'users' => $userRepository->findAll(),
        ]);
    }

    /**
     * @Route("/deleteUser_{id}" , name="userdelete")
     */
    public function delete($id, Request $request, EntityManagerInterface $manager, UserRepository $repository)
    {
        $user = $repository->findOneBy(['email' => $id]);
        $manager->remove($user);
        $manager->flush();
        $this->addFlash('utilisateur_spp', 'supprimer avec success');
        return $this->redirectToRoute('utilisateur');
    }

    /**
     * @Route("/admin_new_admin",name="admin_new_admin")
     * @param AdminRepository $adminRepository
     * @param Request $request
     * @return Response
     */
    public function newAdmin(AdminRepository  $adminRepository, Request $request, UserPasswordEncoderInterface  $encoder, MailerInterface $mailerInterface): Response
    {
        $admin = new Admin();
        $form = $this->createForm(AdminFormType::class, $admin);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $token = hash('sha256', uniqid());
            $admin->setPassword($encoder->encodePassword($admin, $form->get('plainPassword')->getData()))->setRoles(["ROLE_ADMIN"]);
            $admin->setValidationToken($token);
            $admin->setIsValid(false);
            $this->getDoctrine()->getManager()->persist($admin);
            $this->getDoctrine()->getManager()->flush();
            $user = $admin;
            $email = (new TemplatedEmail())
                ->from('newsletter@site.fr')
                ->to($admin->getEmail())
                ->subject('Activation du comtpe ')
                ->htmlTemplate("components/_email.html.twig")
                ->context(compact('user', 'token'));
            $mailerInterface->send($email);
            $this->addFlash('loginemail', 'un mail a été envoyer sur votre adresse consulter le pour achever l\'inscription  ');
            $this->addFlash('admin_email', "un maila a été envoyé pour activé le compte ");
            $this->redirectToRoute('admin_new_admin');
        }
        $admins = $adminRepository->findAll();
        return $this->render('admin/newadmin.html.twig', [
            'admins' => $admins,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/resendmail", name="resend.mail")
     */
    public function reSendEmail(MailerInterface $mailerInterface, UserRepository $userRepository)
    {
        $u = $this->getUser();
        $user =  $userRepository->findOneBy(['email' =>  $u->getUserIdentifier()]);
        $token = $user->getValidationToken();
        if ($user) {
            $email = (new TemplatedEmail())
                ->from('newsletter@site.fr')
                ->to($user->getEmail())
                ->subject('Activation du comtpe ')
                ->htmlTemplate("components/_email.html.twig")
                ->context(compact('user',  'token'));
            $mailerInterface->send($email);

            $this->addFlash("resend", "mail envoyé");

        if ($this->isGranted('ROLE_ADMIN')){
                return $this->redirectToRoute('admin');
        } elseif ($this->isGranted("ROLE_REC")) {
            return $this->redirectToRoute('recruteur');
        } elseif ($this->isGranted("ROLE_CAN")) {
            return $this->redirectToRoute('candidat');
        }
        return  $this->redirectToRoute('candidat');
        }
    }

    /**
     * @Route("/forgetpawword", name="app.forget.email")
     */
    public  function fillEmail(MailerInterface $mailerInterface, UserRepository $userRepository, Request $request)
    {

        $fromBuilder = $this->createFormBuilder()->add('email', EmailType::class, [
            'label' => 'Votre mail'
        ])->getForm();
        $fromBuilder->handleRequest($request);
        if ($fromBuilder->isSubmitted() && $fromBuilder->isValid()) {
            $user =  $userRepository->findOneBy(['email' =>  $fromBuilder->get('email')->getData()]);
           
            if ($user) {
                $token = $user->getValidationToken();
                $email = (new TemplatedEmail())
                    ->from('newsletter@site.fr')
                    ->to($user->getEmail())
                    ->subject('Activation du comtpe ')
                    ->htmlTemplate("components/_forget.html.twig")
                    ->context(['user' => $user , 'token' => $token ]);
                $mailerInterface->send($email);
    
                $this->addFlash("forget_email", "Un mail à été envoyé sur votre boite mail");
            }else {
                $this->addFlash("forget", "Désolé ce mail n'existe pas ");
            }
         
        }

        return $this->render('components/_forgetemail.html.twig', [
            'form' => $fromBuilder->createView(),
        ]);
    }

    
    /**
     * @Route("/changepassword{token}", name="app.forget.password")
     */
    public  function changePassword($token,EntityManagerInterface $manager, MailerInterface $mailerInterface, UserRepository $userRepository, Request $request,UserPasswordEncoderInterface  $encoder)
    { 
        $user = $userRepository->findOneBy(['validation_token' => $token ]); 
      
            $fromBuilder = $this->createFormBuilder()->add('plainPassword', RepeatedType::class, [
                // instead of being set onto the object directly,
                // this is read and encoded in the controller
                'type' => PasswordType::class,
                'invalid_message' => 'Les mots de passe doivent conrespondre.',
                'options' => ['attr' => ['class' => 'password-field']],
                'required' => true,
                'first_options'  => ['label' => 'Mot de passe'],
                'second_options' => ['label' => 'Confirmé le mot de passe'],
                'mapped' => false,
                'constraints' => [
                    new NotBlank([
                        'message' => 'Entrer un mot de passe ',
                    ]),
                    new Length([
                        'min' => 6,
                        'minMessage' => 'votr mot de passe doit est etre qu moins de  {{ limit }} caratères',
                        // max length allowed by Symfony for security reasons
                        'max' => 4096,
                    ]),
                ],
            ])->getForm() ;
            $fromBuilder->handleRequest($request);

            if($fromBuilder->isSubmitted() && $fromBuilder->isValid())
            {
            
                if( $user )
                {
                 $user->setPassword($encoder->encodePassword($user,$fromBuilder->get('plainPassword')->getData()));
                // dd($user);
                $token = hash('sha256', uniqid());
                $user->setValidationToken($token);
                $manager->persist($user);
                $manager->flush();
                $this->addFlash('ch_success','mot passe changer avec success ') ;
                return $this->redirectToRoute('logout');
            }else {
                $this->addFlash('ch_erro','Utilisateur non trouver ') ;
            }
            }
            return $this->render('components/_changepassword.html.twig',[
                'form'=> $fromBuilder->createView(),
            ]);
        
    }
    /**
     * @Route("/admin_update_{email}",name="admin_update_admin")
     */
    public function adminupdate($email, AdminRepository  $adminRepository, Request $request): Response
    {
        $admin = $adminRepository->findOneBy(['email' => $email]);
        $form = $this->createForm(AdminFormType::class, $admin);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $admin->setRoles(['ROLE_ADMIN']);
            $this->getDoctrine()->getManager()->persist($admin);
            $this->getDoctrine()->getManager()->flush();
            $this->redirectToRoute('admin_new_admin');
        }
        $admins = $adminRepository->findAll();
        return $this->render('admin/newadmin.html.twig', [
            'form' => $form->createView(),
            'admins' => $admins,
        ]);
    }

    /**
     * @Route("/admin_delete_admin{email}",name="admin_delete_admin")
     * @param $email
     * @param AdminRepository $adminRepository
     * @return RedirectResponse
     */
    public function admindelete($email, AdminRepository  $adminRepository): RedirectResponse
    {
        $admin = $adminRepository->findOneBy(['email' => $email]);
        if ($admin) {
            $this->addFlash('user_admin_delete', 'admin supprimer');
            $this->getDoctrine()->getManager()->remove($admin);
            $this->getDoctrine()->getManager()->flush();
            return $this->redirectToRoute('admin_new_admin');
        }
        return $this->redirectToRoute('admin_new_admin');
    }

    /**
     * @Route("/admin_list_candidat", name="admin.list.candidats")
     */
    public function candidatList(CandidatRepository $candidatRepository)
    {
        $candidats  = $candidatRepository->findAll();
        return $this->render('admin/user/candidat.html.twig', [
            'candidats' => $candidats,
        ]);
    }
    /**
     * @Route("/admin_list_recruteur", name="admin.list.recruteur")
     */
    public function recruteurList(RecruteurRepository $recruteurRepository)
    {
        return $this->render('admin/user/recruteur.html.twig', [
            'recruteurs' => $recruteurRepository->findAll(),
        ]);
    }

    /**
     * @Route("/csvcandidat", name="csv.candidat")
     */
    public function candidatResponseCSV(CandidatRepository $candidatRepository, CsvController $csvExport)
    {

        $candidats = $candidatRepository->findAll();

        foreach ($candidats as $candidat) {
            $dataConverter[] = [
                "id" => $candidat->getId(),
                "nom" => $candidat->getNom(),
                "contact" => $candidat->getPrenom(),
                "niveau" => $candidat->getNiveau(),
                "metier" => $candidat->getMetier(),
                "pays" => $candidat->getPays(),
                "nombre d\" année dexperience" => $candidat->getAnexp(),

            ];
        };

        return  $csvExport->creatSpreadsheet($dataConverter, "candidatsList");
    }
    /**
     * @Route("/csvrecruteur", name="csv.recruteur")
     */
    public function recruteurReponseCsv(RecruteurRepository $recruteurRepository, CsvController $csvExport)
    {

        $candidats = $recruteurRepository->findAll();

        foreach ($candidats as $candidat) {
            $dataConverter[] = [
                "id" => $candidat->getId(),
                "Nom" => $candidat->getNom(),
                "Contact" => $candidat->getPrenom(),
                "Email" => $candidat->getEmail(),
                "Matricule" => $candidat->getNumimatricul(),
                "Site web " => $candidat->getSiteweb(),
                "Pays" => $candidat->getPays(),
                "Ville" => $candidat->getVille(),


            ];
        };

        return  $csvExport->creatSpreadsheet($dataConverter, "recruteurList");
    }
}
