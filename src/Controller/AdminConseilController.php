<?php


namespace App\Controller;


use App\Entity\Conseil;
use App\Form\ConseilFormType;
use App\Repository\ConseilRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

class AdminConseilController extends \Symfony\Bundle\FrameworkBundle\Controller\AbstractController
{
    private $manager;

    private $repos;

    public function __construct(EntityManagerInterface  $manager,ConseilRepository $conseilRepository)
    {
        $this->manager = $manager;
        $this->repos = $conseilRepository;
    }

    /**
     * @Route("/admin_conseil",name="admin_conseil")
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function  index(Request  $request)
    {
        $conseil = new Conseil();
        $form = $this->createForm(ConseilFormType::class, $conseil);
        $form->handleRequest($request);
        $avi = $this->repos->findAll();
        if($form->isSubmitted() && $form->isValid())
        {
            $this->manager->persist($conseil);
            $this->manager->flush();
            $this->addFlash('conseil_add','Ajouter avec success');
            return  $this->redirectToRoute('admin_conseil');
        }
        return $this->render('admin/conseil/index.html.twig',
            [
                'conseils' => $this->repos->findAll(),
                'form' => $form->createView(),
            ]);
    }

    /**
     * @Route ("/admin_conseil_update{id}",name="admin_conseil_update")
     */
    public function update ($id , Request  $request)
    {
        $conseil = $this->repos->findOneBy(['id' => $id]);
        $form = $this->createForm(ConseilFormType::class,$conseil);
        $form->handleRequest($request);
        if($form->isSubmitted() && $form->isValid())
        {
            $this->manager->persist($conseil);
            $this->manager->flush();
            $this->addFlash('consiel_update','modifier avec success');
            return  $this->redirectToRoute('admin_conseil');
        }
        return $this->render('admin/conseil/index.html.twig',
            [
                'conseils' => $this->repos->findAll(),
                'form' => $form->createView(),
            ]);

    }
    /**
     * @Route ("/admin_conseil_delete{id}",name="admin_conseil_delete")
     */
    public function delete ($id , Request  $request)
    {
        $conseil = $this->repos->findOneBy(['id' => $id]);
        $this->manager->remove($conseil);
        $this->manager->flush();
        return  $this->redirectToRoute('admin_conseil');

    }


}