<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class ErrorPageController extends AbstractController
{
    /**
     * @Route("/404", name="error404")
     */
    public function index(): Response
    {
        return $this->render('error_page/404.html.twig', [
            'controller_name' => 'Erreur 404',
        ]);
    }
}
