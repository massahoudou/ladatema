<?php

namespace App\Controller\candidat;

use App\Entity\Avatar;
use App\Entity\Candidat;
use App\Entity\Competence;
use App\Entity\Cv;
use App\Entity\Formation;
use App\Form\AvatarFormType;
use App\Form\CandidatFormType;
use App\Form\CandidatInoFormType;
use App\Form\CompetenceType;
use App\Form\CvformType;
use App\Form\FormationType;
use App\Repository\AvatarRepository;
use App\Repository\CandidatRepository;
use App\Repository\CompetenceRepository;
use App\Repository\CvRepository;
use App\Repository\FormationRepository;
use App\Repository\UserRepository;
use Doctrine\ORM\EntityManagerInterface;
use phpDocumentor\Reflection\Types\This;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\User\UserInterface;

/**
 * @Route("/profile")
 *
 */
class CandidatController  extends AbstractController
{
    private $repos;
    private $manager;
    private $repository;
    private $cvRepository;
    private $userRepository;
    private $flashy;
    /**
     * @var CompetenceRepository
     */
    private $Crepository;

    public function __construct(CandidatRepository $candidatRepository, EntityManagerInterface $manager, FormationRepository $repository, CompetenceRepository $competenceRepository, CvRepository $cvRepository, UserRepository $userRepository)
    {

        $this->repos = $candidatRepository;
        $this->manager = $manager;
        $this->repository = $repository;
        $this->Crepository = $competenceRepository;
        $this->cvRepository = $cvRepository;
        $this->userRepository = $userRepository;

    }

    /**
     * @Route("/candidat",name="candidat")
     * @param UserInterface $user
     * @return Response
     */
    public function index(UserInterface $user)
    {
        $candidat = $this->repos->findOneBy(['email' => $user->getUsername()]);
        $formation = $this->repository->findForCandidat($candidat);
        $competence = $this->Crepository->findForCandidat($candidat);
        return $this->render('user/candidat/index.html.twig', [
            'controllerName' => 'HI',
            'formations' => $formation,
            'competences' => $competence
        ]);
    }
    /**
     * @Route("/info",name="info")
     * @param Request $request
     * @param UserInterface $user
     * @return Response
     */
    public function info(Request $request, UserInterface $user)
    {
        $candidat = $this->repos->findOneBy(['email' => $user->getUsername()]);
        $form = $this->createForm(CandidatInoFormType::class, $candidat);
        $form->handleRequest($request);
        if ($form->isSubmitted() &&  $form->isValid()) {
         
            $this->manager->persist($candidat);
            $this->manager->flush();
            $this->addFlash('info_modif_candidat', 'modiifer avec success');
            return $this->redirectToRoute('candidat');
        }
        return $this->render('user/candidat/info.html.twig', [
            'form' => $form->createView()
        ]);
    }
    /**
     * @Route("/new_formation",name="new_formation")
     */
    public function formation(Request $request, UserInterface $user)
    {

        $formation = new Formation();
        $form = $this->createForm(FormationType::class, $formation);
        $form->handleRequest($request);
        if ($formation->getDateFin()) {
            if ($formation->getDateDebut()->format("Y-m-d") >= $formation->getDateFin()->format("Y-m-d")) {
            
                $this->addFlash("error_date", "Date de debut superieur ou egal a Date de fin ");
                return $this->render('user/candidat/newf.html.twig', [
                    'form' => $form->createView(),
                ]);
            }
        }

        if ($form->isSubmitted() && $form->isValid()) {
            $candidat = $this->repos->findOneBy(['email' => $user->getUsername()]);
            $formation->setCandidat($candidat);


            $this->manager->persist($formation);
            $this->manager->flush();
            $this->addFlash('info', 'Ajouter avec succes');
            return $this->redirectToRoute('candidat');
        }
        return $this->render('user/candidat/newf.html.twig', [
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/formation_delete{id}" , name="formation_delete")
     */
    public function formationdelete($id, Request $request)
    {
        $formation = $this->repository->findOneBy(['id' => $id]);
        $this->manager->remove($formation);
        $this->manager->flush();
        $this->addFlash('info', 'Suprimer avec success');
        return   $this->redirectToRoute('candidat');
    }

    /**
     * @Route("formation_update_{id}",name="formation_update")
     */
    public function formationUpdate($id, Request $request, UserInterface $user)
    {
        $formation = $this->repository->findOneBy(['id' => $id]);
        $form = $this->createForm(FormationType::class, $formation);
        $form->handleRequest($request);

        if ($formation->getDateFin()) {
            if ($formation->getDateDebut()->format("Y-m-d") >= $formation->getDateFin()->format("Y-m-d")) {
            
                $this->addFlash("error_date", "Date de debut superieur ou egal a Date de fin ");
                return $this->render('user/candidat/newf.html.twig', [
                    'form' => $form->createView(),
                ]);
            }
        }
        if ($form->isSubmitted() && $form->isValid()) {
            $candidat = $this->repos->findOneBy(['email' => $user->getUsername()]);
            $formation->setCandidat($candidat);
            $this->manager->persist($formation);
            $this->manager->flush();
            $this->addFlash('info', 'Ajouter avec succes');
            return $this->redirectToRoute('candidat');
        }
        return $this->render('user/candidat/newf.html.twig', [
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/new_competence",name="new_competence")
     * @param Request $request
     * @param UserInterface $user
     * @return Response
     */
    public function competence(Request $request, UserInterface $user)
    {
        $candidat = $this->repos->findOneBy(['email' => $user->getUsername()]);
        $competence = new Competence();
        $form = $this->createForm(CompetenceType::class, $competence);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $competence->setCandidat($candidat);
            $this->manager->persist($competence);
            $this->manager->flush();
            $this->addFlash('info', 'creer avec success');
            return    $this->redirectToRoute('candidat');
        }
        return $this->render('user/candidat/newc.html.twig', [
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route ("/update_competence{id}",name="update_competence")
     * @param $id
     * @return Response
     */
    public function updatecompetence($id, CompetenceRepository $competenceRepository, Request $request, UserInterface  $user)
    {
        $candidat = $this->repos->findOneBy(['email' => $user->getUsername()]);
        $competence = $competenceRepository->findOneBy(['id' => $id]);
        $form = $this->createForm(CompetenceType::class, $competence);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $competence->setCandidat($candidat);
            $this->manager->persist($competence);
            $this->manager->flush();
            $this->addFlash('info', 'creer avec success');
            return $this->redirectToRoute('candidat');
        }

        return $this->render('user/candidat/newc.html.twig', [
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route ("/delete_competence{id}",name="delete_competence")
     */
    public  function  deletecompetence($id, CompetenceRepository $competenceRepository)
    {
        $competence = $competenceRepository->findOneBy(['id' => $id]);
        if ($competence) {
            $this->manager->remove($competence);
            $this->manager->flush();
            $this->addFlash('deletecompetence', 'supprimer');
            return $this->redirectToRoute('candidat');
        }
        return $this->redirectToRoute('candidat');
    }
    /**
     * @Route("/cv",name="cv")
     */
    public function cv(Request $request, UserInterface $user)
    {
        $cv = new Cv();
        $candidat = $this->repos->findOneBy(['email' => $user->getUsername()]);
        $form = $this->createForm(CvformType::class, $cv);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $cv->setCandidat($candidat);
            $this->manager->persist($cv);
            $this->manager->flush();
            $this->addFlash('cv', 'Votre cv a bien été mis a jour ');
            return $this->redirectToRoute('cv');
        }
        return $this->render('user/candidat/cv.html.twig', [
            'form' => $form->createView(),
            'cv' => $this->cvRepository->findOneBy(['candidat' => $candidat])

        ]);
    }
    /**
     * @Route("/cv_delete" , name="cv_delete")
     */
    public function cvdelete(UserInterface $user)
    {
        $candidat = $this->repos->findOneBy(['email' => $user->getUsername()]);
        $cv = $this->cvRepository->findOneBy(['candidat' => $candidat]);
        $cv->setCandidat(null);
        $this->manager->remove($cv);
        $this->manager->flush();

        return $this->redirectToRoute('cv');
    }
    /**
     * @Route("/photo" , name="photo")
     */
    public function photo(Request $request, UserInterface $user, AvatarRepository $avatarRepository)
    {
        $users = $this->userRepository->findOneBy(['email' => $user->getUsername()]);
        if ($avatarRepository->findOneBy(['users' => $user])) {
            $Avatar = $avatarRepository->findOneBy(['users' => $user]);
        } else {
            $Avatar = new Avatar();
        }
        $form  = $this->createForm(AvatarFormType::class, $Avatar);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $Avatar->setUsers($users);
            $this->manager->persist($Avatar);
            $this->manager->flush();
            return $this->redirectToRoute('photo');
        }
        return $this->render('user/photo.html.twig', [
            'form' => $form->createView()
        ]);
    }

    /**
     * @Route("/deletephoto",name="photo_delete")
     * @return RedirectResponse
     */
    public function photodelete(UserRepository $userRespository, AvatarRepository  $avatarRepository, UserInterface  $user)
    {
        $user = $userRespository->findOneBy(['email' => $user->getUsername()]);
        $avatar = $avatarRepository->findOneBy(['users' => $user]);
        if ($avatar) {
            $avatar->setUsers(null);
            $this->manager->remove($avatar);
            $this->manager->flush();
            return $this->redirectToRoute('photo');
        }
        return $this->redirectToRoute('photo');
    }
}
