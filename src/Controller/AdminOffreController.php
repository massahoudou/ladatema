<?php

namespace App\Controller;

use App\Entity\Offre;
use App\Form\OffreType;
use App\Repository\AdminRepository;
use App\Repository\OffreRepository;
use Doctrine\ORM\EntityManagerInterface;
use Knp\Component\Pager\PaginatorInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\User\UserInterface;

use function PHPSTORM_META\map;

class AdminOffreController  extends AbstractController
{
    private $manager;
    private $repos;
    private $adminrepository;


    public function __construct(EntityManagerInterface $manager,OffreRepository $offreRepository,AdminRepository $adminRepository)
    {
        $this->manager = $manager;
        $this->repos = $offreRepository;
        $this->adminrepository = $adminRepository;
    }

  
    /**
     * @Route("/admin_offre" ,name="admin_offre")
     */
    public function index()
    {
        $offres = $this->repos->findAll();

        return $this->render('admin/offre/index.html.twig',[
            'offres' => $offres,
        ]);
    }
    /**
     * @Route("/admin_offrearchiver" ,name="admin_offre_attente")
     */
    public function attente():Response
    {
        $offres = $this->repos->findattente();
        return $this->render('admin/offre/attente.html.twig',[
            'offres' => $offres,
        ]);
    }
    /**
     * @Route("/admin_offre_valider_{id}" , name="admin_offre_validate")
     */
    public function validate($id):Response
    {
        $offre = $this->repos->findOneBy(['id' => $id ]);
        if ($offre)
        {
             $offre->setArchive(false);
             $this->manager->persist($offre);
             $this->manager->flush();
          return   $this->redirectToRoute('admin_offre_attente');
        }
        $offres = $this->repos->findattente();
        return $this->render('admin/attente.html.twig',[
            'offres' => $offres,
        ]);
    }

    /**
     * @Route("/admin_offre_new", name="admin_offre_new")
     */
    public function new(Request $request, UserInterface $user)
    {
        $offre = new Offre();
        $form = $this->createForm(OffreType::class, $offre);
        $form->handleRequest($request);
        $admin = $this->adminrepository->findOneBy(['email' => $user->getUsername()]);
        if ($form->isSubmitted() && $form->isValid())
        {
            $offre->setUpdateAt(new \DateTime('now'));
            $date = new \DateTime('now') ;
            if($offre->getDelai()->format("Y-m-d") >= $date->format("Y-m-d") )
            {
                $offre->setIsExpired(false);
            }else {
                $offre->setIsExpired(true);
            }

            $offre->setUsers($admin);
            $this->manager->persist($offre);
            $this->manager->flush();
            $this->addFlash('offre_ad_new' , 'Creer  avec success');
            return $this->redirectToRoute('admin_offre');
        }


        return $this->render('admin/offre/new.html.twig',[
            'controllerName' => 'Creer une Offre',
            'form' => $form->createView()
        ]);
    }
    /**
     * @Route("/admin_offfre_update{id}",name="admin_offre_update")
     */
    public function update($id,Request $request, UserInterface $user)
    {
        $offre = $this->repos->findOneBy(['id'=> $id]);
        $form = $this->createForm(OffreType::class,$offre);
        $form->handleRequest($request);
        $admin = $this->adminrepository->findOneBy(['email' => $user->getUsername()]);

        if ($form->isSubmitted() && $form->isValid()) {

            $offre->setUsers($admin);
            $offre->setUpdateAt(new \DateTime('now'));
             $date = new \DateTime('now') ;
            if($offre->getDelai()->format("Y-m-d") > $date->format("Y-m-d") )
            {
                $offre->setIsExpired(false);
            }else {
                $offre->setIsExpired(true);
            }

            $this->manager->persist($offre);
            $this->manager->flush();
            $this->addFlash('offre_ad_update' , 'modifier  avec success');

             return $this->redirectToRoute('admin_offre');
        }
     

        return $this->render('admin/offre/new.html.twig',[
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/admin_offre_delete{id}",name="admin_offre_delete")
     * @param $id
     * @param Request $request
     * @return RedirectResponse
     */
    public function delete ($id,Request $request): RedirectResponse
    {
        $offre = $this->repos->findOneBy(['id' => $id ]);
        if($offre != null )
        {
          $this->manager->remove($offre);
          $this->manager->flush();
          $this->addFlash('offre_ad_delete' , 'Supprimer   avec success');
          return  $this->redirectToRoute('admin_offre');
        }
        $this->addFlash('offre_ad_delete' , 'Erreur de suppression');
        return  $this->redirectToRoute('admin_offre');

    }

}
