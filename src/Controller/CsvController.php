<?php

namespace App\Controller ;

use PhpOffice\PhpSpreadsheet\IOFactory;

use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xls;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\BrowserKit\Response;
use Symfony\Component\HttpFoundation\StreamedResponse;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;
class CsvController extends AbstractController {

    public function exportData($data , $filname )
    {
        // dd($data[0]);
        header('Content-Type: text/csv;');
        header('Content-Disposition: attachment; filename="Export'.$filname.'.csv"');
        $i = 0 ;
        
        foreach($data[0] as $data ){
            if($i == 0 ){
                echo '"'.implode( '";"', array_keys($data)).'"'."\n";
            }
            echo '"'.implode( '";"', $data).'"'."\n";
         $i++ ;
        }
       
        return $this->redirectToRoute('admin.list.admin');
    }


    public function creatSpreadsheet($datas,  $filename ) {
        $spreadsheet = new Spreadsheet() ;
       $sheet = $spreadsheet->getActiveSheet() ;




       $sheet->setCellValue('A1', 'List des candidats ')->mergeCells('A1:D1');

       for ($i = 'A'; $i !=  $spreadsheet->getActiveSheet()->getHighestColumn(); $i++) {
        $spreadsheet->getActiveSheet()->getColumnDimension($i)->setAutoSize(TRUE);
    };

        $title = 0 ;
        foreach($datas as $value){
            if($title == 0 )
            $columnNames = array_keys($value) ;
            $title ++ ;
            break;
        };
       
       $columnLetter = 'A';
       foreach ($columnNames as $columnName) {
           // Allow to access AA column if needed and more
           $columnLetter++;
           $sheet->setCellValue($columnLetter.'2', $columnName);
       }

 
    // dd( $datas);
    $i = 3; // Beginning row for active sheet
    foreach ($datas as $data) {
        $columnLetter = 'A';
        foreach ($data as $value) {
            $columnLetter++ ;
            $sheet->setCellValue($columnLetter.$i, $value);
        }
        $i++;
    }

       
 
    $contentType = 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet';
    $writer = new Xlsx($spreadsheet);
    $response = new StreamedResponse();
    $response->headers->set('Content-Type', $contentType);
    $response->headers->set('Content-Disposition', 'attachment;filename="'.$filename.'xlsx"');
    $response->setPrivate();
    $response->headers->addCacheControlDirective('no-cache', true);
    $response->headers->addCacheControlDirective('must-revalidate', true);
    $response->setCallback(function() use ($writer) {
        $writer->save('php://output');
    });

    return $response;
    }
}