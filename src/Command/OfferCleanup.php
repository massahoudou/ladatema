<?php

namespace App\Command;

use App\Repository\OffreRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;

class OfferCleanup extends Command
{

    private   $repository;
    private $manager;

    protected static $defaultName = 'app:offer:cleanup';

    public function __construct(OffreRepository $repository, EntityManagerInterface $manager)
    {
        $this->repository = $repository;
        $this->manager = $manager;
        parent::__construct();
    }

    protected function configure()
    {
        $this
            ->setDescription('Convert Offer whiche delay is exeded to expired ');
            // ->addOption('dry-run', null, InputOption::VALUE_NONE, 'Dry run');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $io = new SymfonyStyle($input, $output);
      
            $io->note('Archivage des offres qui depasse le delai d\'anttente ');
            $exoffres = $this->repository->findExpired();
            if ($exoffres > 0) {
                foreach ($exoffres as $key => $offre) {
                    if ($offre->getIsExpired() == false || $offre->getIsExpired() == null) {

                        $offre->setIsExpired(true);
                        $this->manager->persist($offre);
                        $this->manager->flush();
                    }
                }
            }
      
        $io->success(sprintf('Operation terminé '. count( $exoffres)));
        return 0;
    }
}
