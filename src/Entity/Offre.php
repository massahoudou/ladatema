<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\HttpFoundation\File\File;
use Vich\UploaderBundle\Mapping\Annotation as vich ;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use App\Repository\OffreRepository;



/**
 * @ORM\Entity(repositoryClass=OffreRepository::class)
 * @Vich\Uploadable
 * @UniqueEntity(fields={"titre"}, message="Titre existant ")
 */
class Offre
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     * @Assert\Length(min=3 , minMessage="Votre titre doit étre correcte > 3")
    * @Assert\Regex(
    * pattern= "/[^#+@=:,;!$\\[\]]+$/A",
    * message= "Pas de nombre ou de caractère spécial ")
     */
    private $titre;

    /**
     * @ORM\Column(type="text")
     * @Assert\Length(min=10 , minMessage="Une Description peut-pas etre inferieur a 3 char !")
     */
    private $description;

    /**
     * @ORM\Column(type="integer")
     * @Assert\GreaterThanOrEqual(1)
     */
    private $nombreposte = 1 ;

    /**
     * @ORM\Column(type="string")
     *
     */
    private $experience;

    /**
     * @ORM\Column(type="string", length=255)
     * @Assert\Length(min=2 , minMessage="nom doit etre supérior a 2 caractère ")
    * @Assert\Regex(
    * pattern= "/[^#&_+@=:,;'$(\/[\])]+$/A",
    * message= "Pas de nombre ou de caractère spécial ")
     */
    private $ville;

    /**
     * @ORM\Column(type="string")
     *
     */
    private $etude;

    /**
     * @ORM\Column(type="date")
     * @Assert\GreaterThan("today",message="Date incorrecte")
     *
     */
    private $delai;

    /**
     * @ORM\ManyToOne(targetEntity=Catcontrat::class, inversedBy="offres")
     */
    private $catcontrat;

    /**
     * @ORM\ManyToMany(targetEntity=Secteur::class, inversedBy="offres")
     */
    private $secteur;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $pays;

    /**
     * @ORM\Column(type="integer", nullable=true)
     *  @Assert\Length(min=3 ,minMessage="salaire incompatible")
     */
    private $salaire;

    /**
     * @ORM\ManyToOne(targetEntity=Recruteur::class, inversedBy="offres")
     */
    private $recruteur;


    /**
     * @ORM\Column(type="string", length=255 , nullable=true)
    * @Assert\Image(
     *     mimeTypes="image/*",
     *     mimeTypesMessage="le type de fichier n'est pas pris en charge "
     * )
     * @var string|null
     */
    private $image;

    /**
     *@Vich\UploadableField(mapping="offre", fileNameProperty="image")
     *@var File|null
     */
    private $imageFile;

    /**
     *
     * @ORM\Column(type="datetime")
     */
    private $updateAt;

    /**
     * @ORM\Column(type="boolean", nullable=true)
     */
    private $archive;


    /**
     * @ORM\ManyToOne(targetEntity=Users::class, inversedBy="offre")
     */
    private $users;

    /**
     * @ORM\Column(type="boolean", nullable=true)
     */
    private $isExpired;



    public function __construct()
    {
        $this->secteur = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getTitre(): ?string
    {
        return $this->titre;
    }

    public function setTitre(string $titre): self
    {
        $this->titre = $titre;

        return $this;
    }

    public function getDescription(): ?string
    {
        return $this->description;
    }

    public function setDescription(string $description): self
    {
        $this->description = $description;

        return $this;
    }

    public function getNombreposte(): ?int
    {
        return $this->nombreposte;
    }

    public function setNombreposte(int $nombreposte): self
    {
        $this->nombreposte = $nombreposte;

        return $this;
    }

    public function getExperience(): ?string
    {
        return $this->experience;
    }

    public function setExperience(string $experience): self
    {
        $this->experience = $experience;

        return $this;
    }


    public function getVille(): ?string
    {
        return $this->ville;
    }

    public function setVille(string $ville): self
    {
        $this->ville = $ville;

        return $this;
    }

    public function getEtude(): ?string
    {
        return $this->etude;
    }

    public function setEtude(string $etude): self
    {
        $this->etude = $etude;

        return $this;
    }

    public function getDelai(): ?\DateTimeInterface
    {
        return $this->delai;
    }

    public function setDelai(\DateTimeInterface $delai): self
    {
        $this->delai = $delai;

        return $this;
    }

    public function getCatcontrat(): ?Catcontrat
    {
        return $this->catcontrat;
    }

    public function setCatcontrat(?Catcontrat $catcontrat): self
    {
        $this->catcontrat = $catcontrat;

        return $this;
    }

    /**
     * @return Collection|secteur[]
     */
    public function getSecteur(): Collection
    {
        return $this->secteur;
    }

    public function addSecteur(Secteur $secteur): self
    {
        if (!$this->secteur->contains($secteur)) {
            $this->secteur[] = $secteur;
        }

        return $this;
    }

    public function removeSecteur(Secteur $secteur): self
    {
        $this->secteur->removeElement($secteur);

        return $this;
    }
    public function __toString()
    {
        return $this->titre;
    }

    public function getPays(): ?string
    {
        return $this->pays;
    }

    public function setPays(string $pays): self
    {
        $this->pays = $pays;

        return $this;
    }

    public function getSalaire(): ?int
    {
        return $this->salaire;
    }

    public function setSalaire(?int $salaire): self
    {
        $this->salaire = $salaire;

        return $this;
    }

    public function getRecruteur(): ?Recruteur
    {
        return $this->recruteur;
    }

    public function setRecruteur(?Recruteur $recruteur): self
    {
        $this->recruteur = $recruteur;

        return $this;
    }



    public function setImageFile(File $image = null)
    {
        $this->imageFile = $image;

        // VERY IMPORTANT:
        // It is required that at least one field changes if you are using Doctrine,
        // otherwise the event listeners won't be called and the file is lost
        if ($image) {
            // if 'updatedAt' is not defined in your entity, use another property
            $this->updateAt = new \DateTime('now');
        }
    }

    public function getImageFile()
    {
        return $this->imageFile;
    }

    public function setImage($image)
    {
        $this->image = $image;
    }

    public function getImage()
    {
        return $this->image;
    }

    public function getUpdateAt(): ?\DateTimeInterface
    {
        return $this->updateAt;
    }

    public function setUpdateAt(?\DateTimeInterface $updateAt): self
    {
        $this->updateAt = $updateAt;

        return $this;
    }

    public function getArchive(): ?bool
    {
        return $this->archive;
    }

    public function setArchive(?bool $archive): self
    {
        $this->archive = $archive;

        return $this;
    }



    public function getUsers(): ?Users
    {
        return $this->users;
    }

    public function setUsers(?Users $users): self
    {
        $this->users = $users;

        return $this;
    }

    public function getIsExpired(): ?bool
    {
        return $this->isExpired;
    }

    public function setIsExpired(?bool $isExpired): self
    {
        $this->isExpired = $isExpired;

        return $this;
    }
    
}
