<?php

namespace App\Entity\Newsletter;


use App\Entity\Email;
use App\Repository\Newsletter\CategoriesRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity(repositoryClass=CategoriesRepository::class)
 */
class Categories
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
    * @Assert\Regex(
    * pattern= "/[^#&_+@=:,;'$(\/[\])]+$/A",
    * message= "Pas de nombre ou de caractère spécial ")
     * @ORM\Column(type="string", length=255)
     */
    private $nom;

 

    /**
     * @ORM\OneToMany(targetEntity=Newsletter::class, mappedBy="categories", orphanRemoval=true)
     */
    private $newsletters;

    /**
     * @ORM\ManyToMany(targetEntity=Email::class, mappedBy="Categories")
     */
    private $emails;

    public function __construct()
    {
        $this->email = new ArrayCollection();
        $this->newsletters = new ArrayCollection();
        $this->emails = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getNom(): ?string
    {
        return $this->nom;
    }

    public function setNom(string $nom): self
    {
        $this->nom = $nom;

        return $this;
    }


   
    /**
     * @return Collection<int, Newsletter>
     */
    public function getNewsletters(): Collection
    {
        return $this->newsletters;
    }

    public function addNewsletter(Newsletter $newsletter): self
    {
        if (!$this->newsletters->contains($newsletter)) {
            $this->newsletters[] = $newsletter;
            $newsletter->setCategories($this);
        }

        return $this;
    }

    public function removeNewsletter(Newsletter $newsletter): self
    {
        if ($this->newsletters->removeElement($newsletter)) {
            // set the owning side to null (unless already changed)
            if ($newsletter->getCategories() === $this) {
                $newsletter->setCategories(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection<int, Email>
     */
    public function getEmails(): Collection
    {
        return $this->emails;
    }

    public function addEmail(Email $email): self
    {
        if (!$this->emails->contains($email)) {
            $this->emails[] = $email;
            $email->addCategory($this);
        }

        return $this;
    }

    public function removeEmail(Email $email): self
    {
        if ($this->emails->removeElement($email)) {
            $email->removeCategory($this);
        }

        return $this;
    }
}
