<?php

namespace App\Entity;


use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Validator\Constraints as Assert;
use App\Repository\RecruteurRepository;
/**
 * @ORM\Entity(repositoryClass=RecruteurRepository::class)
 */
class Recruteur  extends Users
{
 
      /**
     * @ORM\Column(type="string", length=255)
    * @Assert\Regex(
    * pattern= "/[^-0-9#&_+@=:,;!'$(\/[\])]+$/A",
    * message= "Champ non valide")
     * @Assert\Length(min=3, minMessage="ce champ superieur a trois caractère ")
     */
    private $nom;

    /**
     * @ORM\Column(type="string", length=255)
     * @Assert\Regex(
     *     pattern= "/[^-0-9#&_+@=:,;!'$(\/[\])]+$/A",
     *     message="Champ non valide "
     * )
     * @Assert\Length(min=3, minMessage="ce champ superieur à trois caractères ")
     */
    private $prenom;


    /**
     * @ORM\Column(type="string", length=255,nullable=true)
    * @Assert\Regex(
    * pattern= "/^([-a-zA-Z0-9])([^#&_+@=:,;!'$(\/[\])])+$/A",
    * message= "Pas de caractère speciaux ")
     */
    private $adresse;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
    * @Assert\Regex(
    * pattern= "/^[a-zA-Z0-9]+$/A",
    * message= "Pas de nombre ou de caractère spécial ")
     *@Assert\Length(min=3, minMessage="ce champ superieur à 8 caractères ")
     */
    private $nomemtreprise;

    /**
     * @ORM\Column(type="string", length=255 ,nullable=true)
    * @Assert\Regex(
    * pattern= "/^[a-zA-Z0-9]+$/A",
    * message= "Pas de caractère speciaux ")
    * @Assert\Length(min=4, minMessage="ce champ superieur à 8 caractères ")
     */
    private $numimatricul;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $description;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     *@Assert\Length(min=4, minMessage="ce champ superieur a quatre caractères ")
     @Assert\regex("/^https?:\/\/(?:www\.)?[-a-zA-Z0-9@:%._\+~#=]{1,256}\.[a-zA-Z0-9()]{1,6}\b(?:[-a-zA-Z0-9()@:%_\+.~#?&\/=]*)$/")
     * @Assert\Url(
     *    message = "L'URL '{{ valeur }}' n'est pas une URL valide",
     * )
     */
    private $siteweb;


    /**
     * @Assert\EqualTo(propertyPath="password",message="Votre mot de passe de ne correspondent pas !")
     */
    public  $confirmpassword;

    public function __construct()
    {
        $this->offres = new ArrayCollection();
    }



    public function getNomemtreprise(): ?string
    {
        return $this->nomemtreprise;
    }

    public function setNomemtreprise(string $nomemtreprise): self
    {
        $this->nomemtreprise = $nomemtreprise;

        return $this;
    }

    public function getNumimatricul(): ?string
    {
        return $this->numimatricul;
    }

    public function setNumimatricul(string $numimatricul): self
    {
        $this->numimatricul = $numimatricul;

        return $this;
    }

    public function getDescription(): ?string
    {
        return $this->description;
    }

    public function setDescription(string $description): self
    {
        $this->description = $description;

        return $this;
    }

    public function getSiteweb(): ?string
    {
        return $this->siteweb;
    }

    public function setSiteweb(?string $siteweb): self
    {
        $this->siteweb = $siteweb;

        return $this;
    }
    
    public function getNom(): ?string
    {
        return $this->nom;
    }

    public function setNom(string $nom): self
    {
        $this->nom = $nom;

        return $this;
    }

    public function getPrenom(): ?string
    {
        return $this->prenom;
    }

    public function setPrenom(string $prenom): self
    {
        $this->prenom = $prenom;

        return $this;
    }


    public function getAdresse(): ?string
    {
        return $this->adresse;
    }

    public function setAdresse(string $adresse): self
    {
        $this->adresse = $adresse;

        return $this;
    }

    public function __toString() {

        return $this->nom;
    }

    public function serialize() {
        return serialize($this->id);
    }

    public function unserialize($data) {
        $this->id = unserialize($data);
    }

}
