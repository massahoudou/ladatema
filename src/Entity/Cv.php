<?php

namespace App\Entity;

use App\Repository\CvRepository;
use Doctrine\ORM\Mapping as ORM;
use Vich\UploaderBundle\Mapping\Annotation as Vich;
use Symfony\Component\HttpFoundation\File\File;
use Symfony\Component\Validator\Constraints as Assert;


/**
 * @ORM\Entity(repositoryClass=CvRepository::class)
 * @Vich\Uploadable
 */
class Cv
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @var File|null
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $filename;

    /**
     * @Assert\Image(
     *     mimeTypes="application/pdf",
     *     mimeTypesMessage="le type de fichier n'est pas pris en charge "
     * )
     *@Vich\UploadableField(mapping="users_cv", fileNameProperty="filename")
     *@var File|null
     */
    private $file;

    /**
     * @ORM\OneToOne(targetEntity=Candidat::class, inversedBy="cv", cascade={"persist", "remove"})
     */
    private $candidat;


    public function getId(): ?int
    {
        return $this->id;
    }

    
    /**
     * @return string|null
     */
    public function getFilename(): ?string
    {
        return $this->filename;
    }

    /**
     * @param string|null $filename
     * @return Article
     */
    public function setFilename(?string $filename): Cv
    {
        $this->filename = $filename;
        return $this;
    }

    /**
     * @return File|null
     */
    public function getFile(): ?File
    {
        return $this->file;
    }

    /**
     * @param File|null $imagefile
     * @return Article
     */
    public function setFile(?File $file = null ): Cv
    {
        $this->file = $file;
        if(null !== $file)
        {
           
        }
        return $this;
    }

    public function getCandidat(): ?Candidat
    {
        return $this->candidat;
    }

    public function setCandidat(?Candidat $candidat): self
    {
        $this->candidat = $candidat;

        return $this;
    }

}
