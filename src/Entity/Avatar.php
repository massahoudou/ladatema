<?php

namespace App\Entity;

use App\Repository\AvatarRepository;
use Doctrine\ORM\Mapping as ORM;
use Vich\UploaderBundle\Mapping\Annotation as Vich;
use Symfony\Component\HttpFoundation\File\File;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity(repositoryClass=AvatarRepository::class)
 * @Vich\Uploadable
 */
class Avatar
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;
    /**
     *
     * @ORM\Column(type="string", length=255,nullable=true)
     */
    private $filename;

    /**
     * @var File
     * @Assert\Image(
     *     mimeTypes="image/*",
     *     mimeTypesMessage="le type de fichier n'est pas pris en charge "
     * )
     * @Vich\UploadableField(mapping="users", fileNameProperty="filename")
     */
    private $imagefile;

    /**
     * @ORM\OneToOne(targetEntity=Users::class, inversedBy="avatar", cascade={"persist", "remove"})
     */
    private $users;

    public function getId(): ?int
    {
        return $this->id;
    }
    public function getUsers(): ?Users
    {
        return $this->users;
    }

    public function setUsers(?Users $users): self
    {
        $this->users = $users;

        return $this;
    }
   
    /**
     * @return string|null
     */
    public function getFilename(): ?string
    {
        return $this->filename;
    }

    /**
     * @param string|null $filename
     * @return Article
     */
    public function setFilename(?string $filename): Avatar
    {
        $this->filename = $filename;
        return $this;
    }

    /**
     * @return File|null
     */
    public function getImagefile(): ?File
    {
        return $this->imagefile;
    }

    /**
     * @param File|null $imagefile
     * @return Article
     */
    public function setImagefile(?File $imagefile = null ): Avatar
    {
        $this->imagefile = $imagefile;
        if(null !== $imagefile)
        {
           
        }
        return $this;
    }
    public function serialize()
    {
        return serialize( array(
            $this->id,
            $this->imagefile,
        ));
    }
}
