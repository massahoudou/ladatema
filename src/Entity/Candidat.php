<?php

namespace App\Entity;

use App\Repository\CandidatRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Vich\UploaderBundle\Mapping\Annotation as Vich;
use Symfony\Component\HttpFoundation\File\File;

/**
 * @ORM\Entity(repositoryClass=CandidatRepository::class)
 * @Vich\Uploadable()
 */
class Candidat extends Users
{
  

    /**
     * @ORM\Column(type="string", length=255)
    * @Assert\Regex(
    * pattern= "/[^-0-9#&_+@=:,;!'$(\/[\])]+$/A",
    * message= "Pas de nombre ou de caractère spécial ")
    */
    private $nom;

    /**
     * @ORM\Column(type="string", length=255)
    * @Assert\Regex(
    * pattern= "/[^-0-9#&_+@=:,;!'$(\/[\])]+$/A",
    * message= "Pas de nombre ou de caractère spécial ")
     */
    private $prenom;

 

    /**
     * @ORM\OneToMany(targetEntity=Formation::class, mappedBy="candidat")
     */
    private $formations;

    /**
     * @ORM\OneToMany(targetEntity=Competence::class, mappedBy="candidat", orphanRemoval=true)
     */
    private $competences;

    /**
     * @Assert\EqualTo(propertyPath="password",message="Votre mot de passe de ne correspondent pas !")
     */
    public  $confirmpassword;

    /**
     * @ORM\Column(type="string", nullable=true)
     */
    private $niveau;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
    * @Assert\Regex(
    * pattern= "/[^0-9#&_+@=:,;$(\/[\])]+$/A",
    * message= "Pas de nombre ou de caractères spéciaux ")
     */
    private $metier;

    /**
     * @ORM\OneToOne(targetEntity=Cv::class, mappedBy="candidat", cascade={"persist", "remove"})
     */
    private $cv;


    /**
     * @ORM\Column(type="string", nullable=true)
     */
    private $anexp;

    /**
     * @ORM\ManyToMany(targetEntity=Secteur::class, inversedBy="candidats")
     */
    private $secteur;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $description;

    /**
     * @ORM\Column(type="boolean" )
     */
    private $isavailable = false ;


    public function __construct()
    {
        $this->formations = new ArrayCollection();
        $this->competences = new ArrayCollection();
        $this->secteur = new ArrayCollection();
    }


    public function getNom(): ?string
    {
        return $this->nom;
    }

    public function setNom(string $nom): self
    {
        $this->nom = $nom;

        return $this;
    }

    public function getPrenom(): ?string
    {
        return $this->prenom;
    }

    public function setPrenom(string $prenom): self
    {
        $this->prenom = $prenom;

        return $this;
    }

    /**
     * @return Collection|Formation[]
     */
    public function getFormations(): Collection
    {
        return $this->formations;
    }

    public function addFormation(Formation $formation): self
    {
        if (!$this->formations->contains($formation)) {
            $this->formations[] = $formation;
            $formation->setCandidat($this);
        }

        return $this;
    }

    public function removeFormation(Formation $formation): self
    {
        if ($this->formations->removeElement($formation)) {
            // set the owning side to null (unless already changed)
            if ($formation->getCandidat() === $this) {
                $formation->setCandidat(null);
            }
        }

        return $this;
    }
    public function __toString()
    {
        return $this->getEmail();
    }

    /**
     * @return Collection|Competence[]
     */
    public function getCompetences(): Collection
    {
        return $this->competences;
    }

    public function addCompetence(Competence $competence): self
    {
        if (!$this->competences->contains($competence)) {
            $this->competences[] = $competence;
            $competence->setCandidat($this);
        }

        return $this;
    }

    public function removeCompetence(Competence $competence): self
    {
        if ($this->competences->removeElement($competence)) {
            // set the owning side to null (unless already changed)
            if ($competence->getCandidat() === $this) {
                $competence->setCandidat(null);
            }
        }

        return $this;
    }


    public function getNiveau(): ?string
    {
        return $this->niveau;
    }

    public function setNiveau(?string $niveau): self
    {
        $this->niveau = $niveau;

        return $this;
    }

    public function getMetier(): ?string
    {
        return $this->metier;
    }

    public function setMetier(?string $metier): self
    {
        $this->metier = $metier;

        return $this;
    }

    public function getCv(): ?Cv
    {
        return $this->cv;
    }

    public function setCv(?Cv $cv): self
    {
        // unset the owning side of the relation if necessary
        if ($cv === null && $this->cv !== null) {
            $this->cv->setCandidat(null);
        }

        // set the owning side of the relation if necessary
        if ($cv !== null && $cv->getCandidat() !== $this) {
            $cv->setCandidat($this);
        }

        $this->cv = $cv;

        return $this;
    }

    public function getAnexp(): ?string
    {
        return $this->anexp;
    }

    public function setAnexp(?string $anexp): self
    {
        $this->anexp = $anexp;

        return $this;
    }

    /**
     * @return Collection|secteur[]
     */
    public function getSecteur(): Collection
    {
        return $this->secteur;
    }

    public function addSecteur(secteur $secteur): self
    {
        if (!$this->secteur->contains($secteur)) {
            $this->secteur[] = $secteur;
        }

        return $this;
    }

    public function removeSecteur(secteur $secteur): self
    {
        $this->secteur->removeElement($secteur);

        return $this;
    }

    public function getDescription(): ?string
    {
        return $this->description;
    }

    public function setDescription(?string $description): self
    {
        $this->description = $description;

        return $this;
    }
    public function serialize() {
        return serialize($this->id);
    }

    public function unserialize($data) {
        $this->id = unserialize($data);
    }

    public function getIsavailable(): ?bool
    {
        return $this->isavailable;
    }

    public function setIsavailable(bool $isavailable): self
    {
        $this->isavailable = $isavailable;

        return $this;
    }
}
