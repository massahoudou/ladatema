<?php

namespace App\Entity;

use App\Repository\AvisRepository;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Symfony\Component\HttpFoundation\File\File;
use Vich\UploaderBundle\Mapping\Annotation as Vich;
use Symfony\Component\Validator\Constraints as Assert;


/**
 * @ORM\Entity(repositoryClass=AvisRepository::class)
 * @Vich\Uploadable
 * @UniqueEntity(fields={"prenom"}, message="prenom existant ")
 */
class Avis
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
    * @Assert\Regex(
    * pattern= "/[^#&_+@=:,;'$(\/[\])]+$/A",
    * message= "Pas de nombre ou de caractère spécial ")
     */
    private $nom;

    /**
     * @ORM\Column(type="string", length=255)
    * @Assert\Regex(
    * pattern= "/[^#&_+@=:,;'$(\/[\])]+$/A",
    * message= "Pas de nombre ou de caractère spécial ")
     */
    private $prenom;

    /**
     * @ORM\Column(type="string", length=255 , nullable=true)
     * @var string|null
     */
    private $image;

    /**
     *@Vich\UploadableField(mapping="avis", fileNameProperty="image")
     * @Assert\Image(
     *     mimeTypes="image/*",
     *     mimeTypesMessage="le type de fichier n'est pas pris en charge "
     * )
     *@var File|null
     */
    private $imageFile;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $commentaire;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getNom(): ?string
    {
        return $this->nom;
    }

    public function setNom(string $nom): self
    {
        $this->nom = $nom;

        return $this;
    }

    public function getPrenom(): ?string
    {
        return $this->prenom;
    }

    public function setPrenom(string $prenom): self
    {
        $this->prenom = $prenom;

        return $this;
    }

    public function getCommentaire(): ?string
    {
        return $this->commentaire;
    }

    public function setCommentaire(string $commentaire): self
    {
        $this->commentaire = $commentaire;

        return $this;
    }
    public function setImageFile(File $image = null)
    {
        $this->imageFile = $image;

        // VERY IMPORTANT:
        // It is required that at least one field changes if you are using Doctrine,
        // otherwise the event listeners won't be called and the file is lost
        //if ($image) {
        // if 'updatedAt' is not defined in your entity, use another property

        //}
    }
    public function getImageFile()
    {
        return $this->imageFile;
    }

    public function setImage($image)
    {
        $this->image = $image;
    }

    public function getImage()
    {
        return $this->image;
    }
}
