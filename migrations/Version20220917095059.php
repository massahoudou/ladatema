<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20220917095059 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE candidat DROP pays');
        $this->addSql('ALTER TABLE offre ADD users_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE offre ADD CONSTRAINT FK_AF86866F67B3B43D FOREIGN KEY (users_id) REFERENCES users (id)');
        $this->addSql('CREATE INDEX IDX_AF86866F67B3B43D ON offre (users_id)');
        $this->addSql('ALTER TABLE recruteur DROP ville');
        $this->addSql('ALTER TABLE users ADD ville VARCHAR(255) DEFAULT NULL, ADD pays VARCHAR(255) DEFAULT NULL, ADD sexe VARCHAR(255) DEFAULT NULL');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE candidat ADD pays VARCHAR(255) DEFAULT NULL');
        $this->addSql('ALTER TABLE offre DROP FOREIGN KEY FK_AF86866F67B3B43D');
        $this->addSql('DROP INDEX IDX_AF86866F67B3B43D ON offre');
        $this->addSql('ALTER TABLE offre DROP users_id');
        $this->addSql('ALTER TABLE recruteur ADD ville VARCHAR(255) DEFAULT NULL');
        $this->addSql('ALTER TABLE users DROP ville, DROP pays, DROP sexe');
    }
}
